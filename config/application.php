<?php
if (!class_exists("MainConfig")) {
    class MainConfig
    {
        public static $config;

        public static function readConfig($config_file)
        {
            $parser = new Spyc();

            if (!file_exists($config_file)) {
                throw new Exception("config_file file not exists.");
            }
            try {
                MainConfig::$config = $parser->loadFile($config_file);
            } catch (Exception $e) {
                echo $e->getMessage();
            }

        }
    }
}

