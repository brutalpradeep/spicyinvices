<?php
/**
 * Created by PhpStorm.
 * User: Naresh
 * Date: 9/2/2015
 * Time: 2:01 PM
 */
namespace App\Lib;
use Illuminate\Support\Facades\Mail;
class Email{

    public function queueEmail($data_object,$email_object) {


        Mail::queueOn('high',$data_object['view'], $data_object['data'], function($message) use($email_object) {
            $message->to($email_object['to'], $email_object['name'])->subject($email_object['subject'])->from($email_object['from_email'],$email_object['from_name']);
        });
    }
}