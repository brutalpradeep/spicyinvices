<?php
namespace App\Lib;

use DEFT\Shopify;
class WebHooks {

    protected $caller;
    protected $webhookHelper;
    protected $queue;
    protected $address;
    public function __construct($shop,$token)
    {
        $this->caller = new Shopify\ShopifyDialer();
        $this->caller->setShopName($shop);
        $this->caller->setAccessToken($token);
        $this->webhookHelper  = new Shopify\WebhookHelper($this->caller);
        $this->queue = new Shopify\QueueOnEvents\Installed();
    }

    public function createWebHook($data){

        $dta = array(
            'webhook' => array(
                "topic" => $data,
                "address" => $this->address."/".$data,
                "format" => "json")
        );
        $this->queue->pushQueue($this->webhookHelper)->createWebHook($dta);
    }

    public function setAddress($address){
        $this->address = $address;
    }
    public function getAddress(){
       return $this->address ;
    }
    public function executeWebHook(){

        return $this->queue->executeQueue();
    }


}