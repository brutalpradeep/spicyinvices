<?php
namespace App\Lib;

use App\Models;
use Illuminate\Support\Facades\Log;


class UpdateOrder 
{

    protected $ordersToUpdate;
    protected $ordersToCreate;


    protected $customersToUpdate;
    protected $customersToCreate;
    
    
    //holds the instance id to find problem.
    protected $instance_id;


    //shop id
    protected $shopId;
    //if it is coming from hook or not
    protected $hook = false;


    protected $delayBetweenBigQueries = 1;
    //Giveup after n exception (hardcoded for now)
    protected $maxTolerateExceptions = 10;

    //Current exceptions count
    protected $currentExceptions = 0;

    //maintains known orders
    protected $knownOrders = array();
    //maintains known customers
    protected $knownCustomers = array();

    //Stores emails of the customers

    protected $shopifyCustomersIds = array();
    //Stores order ids
    protected $shopifyOrdersIds = array();
    //Stores orders coming from shopify
    protected $shopifyOrders = array();
    //Stores customers in orders of shopify
    protected $shopifyCustomers = array();

    public function __construct($shopifyOrders, $shopId = 1, $hook = false)
    {
		$shopifyOrders = $shopifyOrders->orders;
		$this->shopId = $shopId;
		$this->hook = $hook;
        $this->instance_id = str_random(40);
        if ((int)$shopId < 1) {
           // throw new \Exception("Shop id is invalid");
        }
        $all_orders_id = $all_customers = array();
        $this->shopifyOrders = $shopifyOrders;
        $this->collectCustomersAndOrders();

        $this->findKnown();



    }

    protected function collectCustomersAndOrders()
    {

        $this->shopifyOrdersIds = $this->collectOrders();
        list($this->shopifyCustomersIds, $this->shopifyCustomers) = $this->collectCustomersFromShopifyOrders($this->shopifyOrders);

    }

    protected function collectOrders()
    {
        //collect order ids
        return $this->collect($this->shopifyOrders, "id");
    }

    protected function collect($objectOrArrayCollection, $collect)
    {
        $collections = array();
        foreach ($objectOrArrayCollection as $objectOrArray) {

            if (is_array($objectOrArray) && isset($objectOrArray[$collect])) {
                $collections[] = $objectOrArray[$collect];
            } elseif (is_object($objectOrArray) && isset($objectOrArray->$collect)) {
                $collections[] = $objectOrArray->$collect;
            }
        }
        return $collections;
    }

    protected function collectCustomersFromShopifyOrders()
    {
        //in case logic changes
	
        return array(
            array_unique(array_merge($this->collectCustomersFromShopifyCustomers(),$this->collect($this->shopifyOrders, "id"))),
            $this->collect($this->shopifyOrders, "customer")
        );

    }
	
	protected function collectCustomersFromShopifyCustomers()
    {
        //in case logic changes
		$ids= array();
		foreach ($this->shopifyOrders as $order) {

           if(isset($order->customer)){
                $customerData = $order->customer;
                if (is_array($order->customer)) {
                    $customerData = (object)$order->customer;
                }
               $ids[] = $customerData->id;
				
               
            }

        }
        return $ids;

    }

    //know orders

    protected function findKnown()
    {
        $this->findKnownOrders($this->shopifyOrdersIds);
        $this->findKnownCustomers($this->shopifyCustomersIds);

    }

    //known customers

    protected function findKnownOrders($orderIds)
    {
        $knownOrders = Models\Orders::whereIn('order_id', $orderIds)->get();

        return $this->knownOrders = $this->collectColumn($knownOrders, "order_id");
    }

    protected function collectColumn($objectOrArrayCollection, $collect)
    {
        $collections = array();

        foreach($objectOrArrayCollection as $objectOrArray) {

            $name=$this->getColumn($objectOrArray,$collect);

            $collections[$name] = $objectOrArray;

        }

        return $collections;
    }

    protected function getColumn($objectOrArray, $collect)
    {
            if (is_array($objectOrArray) && isset($objectOrArray[$collect])) {
                $column=$objectOrArray[$collect];
            } elseif (is_object($objectOrArray) && isset($objectOrArray->$collect)) {
                $column=$objectOrArray->$collect;
            }
        return $column;
    }

    protected function findKnownCustomers($customerIds)
    {
        $knownCustomers = Models\Customer::whereIn('id', $customerIds)->get();
        $this->knownCustomers = $this->collectColumn($knownCustomers, "id");
    }

    //Update existing order

    public function createOrUpdateOrders()
    {
        $orders = (array)$this->shopifyOrders;
        if (isset($orders)) {
            array_map(function ($order) {
                $this->classifyOrder($order);
            }, $orders);
        }
        if ($this->hook){
            $this->updateAllOrder();
        }
        $this->createAllOrder();
        $this->createOrUpdateCustomers();


    }

    //Insert All new orders

    protected function classifyOrder($order)
    {

        if (isset($this->knownOrders[$order->id])) {
            $this->ordersToUpdate[$order->id] = $order;
        } else {
            $this->ordersToCreate[$order->id] = $order;
        }
        $this->knownOrders[$order->id] = $order;
    }

    protected function updateAllOrder()
    {
        $orders = $this->ordersToUpdate;


        if ($orders) {
            foreach ($orders as $order) {
                try {
                    $customer_id =0;
                    if(isset($order->customer)){
                        $customer_id =   $this->getColumn($order->customer, "id");

                    }
                    Models\Orders::where('order_id', $order->id)
                        ->update(['order_id' => $order->id,
                            'email' => $order->email,
                            'order_created_at' => $order->created_at,
                            'order_updated_at' => $order->updated_at,
                            'name' => $order->name,
                            'total_price' => $order->total_price,
                            'processed_at' => $order->processed_at,
                            'fulfillment_status' => ($order->fulfillment_status != '') ? $order->fulfillment_status : '',
							'financial_status' => ($order->financial_status != '') ? $order->financial_status : '',
                            'status' => ($order->cancelled_at != '') ? 'cancelled' : ($order->closed_at != '') ? 'closed' : 'open',
                            'currency' => $order->currency,
                            'data' => json_encode($order),
                            'instance_id' => $this->instance_id,
                            'customer_id' => $customer_id

                        ]);
                } catch (\Exception $e) {
                    $this->logOrGiveUp($e);
                }
            }
        }
        return true;
    }

    protected function logOrGiveUp(\Exception $e)
    {
        Log::error($this->currentExceptions);
        if ($this->currentExceptions > $this->maxTolerateExceptions) {
            die($e->getMessage());
        }
        $this->currentExceptions++;

    }

    //Update existing customers

    protected function createAllOrder()
    {
        $orders = $this->ordersToCreate;

        if ($orders) {
            foreach ($orders as $order) {
                $customer_id =0;
                if(isset($order->customer)){
                    $customer_id =   $this->getColumn($order->customer, "id");

                }
                $order_data[] = array('order_id' => $order->id,
                    'email' => $order->email,
                    'order_created_at' => $order->created_at,
                    'order_updated_at' => $order->updated_at,
                    'name' => $order->name,
                    'total_price' => $order->total_price,
                    'processed_at' => $order->processed_at,
                    'fulfillment_status' => ($order->fulfillment_status != '') ? $order->fulfillment_status : '',
                    'status' => ($order->cancelled_at != '') ? 'cancelled' : ($order->closed_at != '') ? 'closed' : 'open',
                    'financial_status' => ($order->financial_status != '') ? $order->financial_status : '',
                    'shop_id' => ($this->shopId != '') ? $this->shopId : 1,
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                    //$order_new->custom_status_id=0;
                    'data' => json_encode($order),
                    'currency' => $order->currency,
                    'instance_id' => $this->instance_id,
                    'customer_id' => $customer_id
                );

            }

            if (isset($order_data)) {
                $i = 0;
                while ($data = array_slice($order_data, $i * 100, 100)) {
                    try {
                        $order_new = Models\Orders::insert($data);
                        $i++;
                        sleep($this->delayBetweenBigQueries);
                        //usleep($this->config['Sleep']['MICROSECONDS']);
                    } catch (\Exception $e) {
                        $this->logOrGiveUp($e);

                    }
                }
            }


        }
        return true;
    }


    //Insert All new Customers

    protected function createOrUpdateCustomers()
    {
        $all_cus = (array)$this->shopifyCustomers;

        array_map(function ($customer) {

            $this->createOrUpdateCustomer($customer);
        }, $all_cus);
        if ($this->hook){
            $this->updateAllCustomers();
        }
        $this->createAllCustomers();

    }

    protected function createOrUpdateCustomer($customer)
    {
        if (is_array($customer)) {
            $customer = (object)$customer;
        }
        if (isset($this->knownCustomers[$customer->id]) && $this->knownCustomers !='' ) {

            $this->customersToUpdate[$customer->id] = $customer;
        } else {

            $this->customersToCreate[$customer->id] = $customer;
        }


        $this->knownCustomers[$customer->id] = $customer;
    }

    public function updateAllCustomers()
    {
        $customerDatas = $this->customersToUpdate;

        if ($customerDatas) {
            foreach ($customerDatas as $customerData) {

                try {
                    $customer = Models\Customer::firstOrCreate(array('id' => $customerData->id));

                         $customer->id = $customerData->id;
                         $customer->email = $customerData->email;
                         $customer->first_name =  ($customerData->first_name != '') ? $customerData->first_name : '';
                         $customer->last_name =  ($customerData->last_name != '') ? $customerData->last_name : '';
                         $customer->name =  $customerData->first_name . ' ' . $customerData->last_name;
                         $customer->instance_id =  $this->instance_id;
                         $customer->save();

                } catch (\Exception $e) {
                    $this->logOrGiveUp($e);
                }
            }
        }
    }

    protected function createAllCustomers()
    {

        $customerDatas = $this->customersToCreate;

        if ($customerDatas) {
            foreach ($customerDatas as $customerData) {
                //Log::error('hiii....'.json_encode($customerData));

                $customer_data[] = array(
                    'id' => $customerData->id,
                    'email' => $customerData->email,
                    //$customer->name = $customerData->first_name.' '.$order->customer->last_name;
                    'first_name' => ($customerData->first_name != '') ? $customerData->first_name : '',
                    'last_name' => ($customerData->last_name != '') ? $customerData->last_name : '',
                    'name' => $customerData->first_name . ' ' . $customerData->last_name,
                    'shop_id' => ($this->shopId != '') ? $this->shopId : 1,
                    'created_at' => date('Y-m-d G:i:s'),
                    'updated_at' => date('Y-m-d G:i:s'),
                    'instance_id' => $this->instance_id
                );

            }
            if (isset($customer_data)) {
                $i = 0;
                while ($data = array_slice($customer_data, $i * 100, 100)) {
                    try {
                        $customer_new = Models\Customer::insert($data);
                        $i++;
                        usleep($this->delayBetweenBigQueries);

                    } catch (\Exception $e) {$this->logOrGiveUp($e);
                        //Log::error($e->getMessage());
                    }
                }

            }
        }
    }

}
