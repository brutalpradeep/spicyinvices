<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\MyMigration::class,
        \App\Console\Commands\CheckQueue::class,
        \App\Console\Commands\DBStatus::class,
        \App\Console\Commands\QueueJobs::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
		$schedule->command('MyMigration')->everyMinute();
		$schedule->command('QueueJobs')->cron('0 */6 * * *');
    }
}
