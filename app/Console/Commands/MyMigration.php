<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MyMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MyMigration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	protected $config; 
    public function __construct()
    {
        parent::__construct();
		$this->config =  \MainConfig::$config;
		
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = array('default' => env('DB_CONNECTION', $this->config['DB']['DB_DATABASE']),
            'connections'=>array($this->config['DB']['DB_DATABASE'] => [
                'driver'    => 'mysql',
                'host'      => $this->config['DB']['DB_HOST'],
                'database'  => $this->config['DB']['DB_DATABASE'],
                'username'  => $this->config['DB']['DB_USERNAME'],
                'password'  => $this->config['DB']['DB_PASSWORD'],
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]));
            Config::set('database', $connection);


           if(!Schema::hasTable('shop_users')){
               Schema::create('shop_users', function (Blueprint $table) {
                   $table->increments('id');
                   $table->string('shop_name');
                   $table->string('permanent_token');
                   $table->string('database_name');
                   $table->tinyInteger('is_active')->default(1);
                   $table->timestamps();
               });


               for($i=0;$i<50;$i++){
                 $db =  $this->createDb();

               }
        }
        Config::set('database', $connection);
        $columns = Schema::getColumnListing('shop_users');
        if(!in_array('payment_status',$columns)){
            Schema::table('shop_users', function($table)
            {
                $table->enum('payment_status', array('pending', 'accepted','declined','expired'));
            });
        }


		$GLOBALS['database'] = $this->config['DB']['DB_DATABASE'];
        $shops =Models\ShopUsers::where('is_active',1)->get();
		$b_shops =Models\ShopUsers::where('shop_name','=','')->get();
        $count = $b_shops->count();
        
        for($i=0;$i<(50-$count);$i++){
            $db =  $this->createDb();
        }

         foreach($shops as $shop){

             $database = $shop->database_name;
            // $GLOBALS['database'] = $database;
             $this->runMigration($database);
         }
    }
    function createDb(){
      try{
        $databasename ='cos_'. md5(uniqid(rand(), true));
        DB::statement('CREATE DATABASE IF NOT EXISTS `'. $databasename.'`');
        $this->runMigration($databasename);
        $GLOBALS['database'] = $this->config['DB']['DB_DATABASE'];
       // Config::set('database', $connection);
        $db = new  Models\ShopUsers();
        $db->database_name =$databasename;
        $db->save();
      }catch (Exception $e) {
          echo 'Caught exception: ',  $e->getMessage(), "\n";
      }

    }
    
    function runMigration($database){
        $connection =  array('default' => env('DB_CONNECTION',$database),
            'connections'=>array($database => [
                'driver'    => 'mysql',
                'host'      => $this->config['DB']['DB_HOST'],
                'database'  => $database,
                'username'  => $this->config['DB']['DB_USERNAME'],
                'password'  => $this->config['DB']['DB_PASSWORD'],
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]));

             Config::set('database', $connection);

             Artisan::call('migrate', [
                 '--database'=>$database,
                 '--force' => true
             ]);
    }
}
