<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Contracts\Bus\SelfHandling;
class CreateDb extends Command implements SelfHandling
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateDb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';
    protected $db;
    protected $config;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($db)
    {

        parent::__construct();
        $this->db = $db;
        $this->config =  \MainConfig::$config;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
          $databasename = $this->db;
            $connection =  array('default' => env('DB_CONNECTION', $this->config['DB']['DB_DATABASE']),
                'connections'=>array($this->config['DB']['DB_CONNECTION'] => [
                    'driver'    => 'mysql',
                    'host'      => $this->config['DB']['DB_HOST'],
                    'database'  => $this->config['DB']['DB_DATABASE'],
                    'username'  => $this->config['DB']['DB_USERNAME'],
                    'password'  => $this->config['DB']['DB_PASSWORD'],
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                    'strict'    => false,
                ]));
            Config::set('database', $connection);

            DB::statement('CREATE DATABASE IF NOT EXISTS `'. $databasename.'`');
            
             
            $db = new  Models\ShopUsers();
            $db->database_name =$databasename;
            $db->save();
            
             $connection2 =  array('default' => env('DB_CONNECTION',$databasename),
            'connections'=>array($databasename => [
                'driver'    => 'mysql',
                'host'      => $this->config['DB']['DB_HOST'],
                'database'  => $databasename,
                'username'  => $this->config['DB']['DB_USERNAME'],
                'password'  => $this->config['DB']['DB_PASSWORD'],
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]));

           
            
             Config::set('database', $connection2);

             Artisan::call('migrate', [
                 '--database'=>$databasename,
                 '--force' => true
             ]);
           
            $db = new  Models\Shop();
            $db->database_name =$databasename;
            $db->save();

        }
        catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

}
