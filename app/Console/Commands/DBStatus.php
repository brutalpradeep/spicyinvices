<?php

namespace App\Console\Commands;

use App\Models;
use Illuminate\Console\Command;;

class DBStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DBStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->config =  \MainConfig::$config;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $GLOBALS['database']=$this->config['DB']['DB_DATABASE'];
        $all_db = Models\ShopUsers::all()->count();
        $blank_db = Models\ShopUsers::where('shop_name','')->where('permanent_token','')->count();
        $active_db = Models\ShopUsers::where('shop_name','!=','')->where('permanent_token','!=','')->where('is_active',1)->count();
        $all_jobs =Models\Jobs::all()->count();

        echo '==================================='.PHP_EOL;
        echo '||  All Databases    ||  '.$all_db.PHP_EOL;
        echo '||  Blank Databases  ||  '.$blank_db.PHP_EOL;
        echo '||  Active Databases ||  '.$active_db.PHP_EOL;
        echo '||  All Jobs         ||  '.$all_jobs.PHP_EOL;
        echo '==================================='.PHP_EOL;
    }
}
