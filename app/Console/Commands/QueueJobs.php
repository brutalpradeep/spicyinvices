<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class QueueJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'QueueJobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $config;
    public function __construct()
    {
        parent::__construct();
        $this->config =  \MainConfig::$config;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = array('default' => env('DB_CONNECTION', $this->config['DB']['DB_DATABASE']),
            'connections'=>array($this->config['DB']['DB_DATABASE'] => [
                'driver'    => 'mysql',
                'host'      => $this->config['DB']['DB_HOST'],
                'database'  => $this->config['DB']['DB_DATABASE'],
                'username'  => $this->config['DB']['DB_USERNAME'],
                'password'  => $this->config['DB']['DB_PASSWORD'],
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',

                'strict'    => true,
            ]));

        Config::set('database', $connection);
        $jobs = DB::table('jobs')->get();
        $data['TestEmail']=(isset($this->config['TestEmail']))?$this->config['TestEmail']:'codevarun@gmail.com';
		$domain = (isset($this->config['env']))? $this->config['env']:'prod';
        if(count($jobs)>0 && $domain =='prod'){


            $html= '<h3>No. Of Jobs Pending:<b>'.count($jobs).'</b></h3><table border="1"><tbody><th>S.No.</th><th>ID</th><th>ATTEMPTS</th><th>RESERVED</th>';
            $i=1;
            foreach($jobs as $job ){
                $html .= '<tr><td border="1">'.$i.'</td><td border="1" width="100">'.$job['id'].'</td><td border="1">'.$job['attempts'].'</td><td border="1">'.$job['reserved'].'</td></tr>';
                $i++;

            }
            $html .= '</tbody></table>';
            $data['html']=$html;
            Mail::send(array(), array(), function($message)  use ($data) {
                $message->to($data['TestEmail'], 'Varun')->subject('Custom Status App - Pending Jobs Status')->from('support@spicegems.com','RMS Jobs Status')->setBody($data['html'], 'text/html');;
            });

        }
       // echo '<pre>';print_r($users);exit;
    }
}
