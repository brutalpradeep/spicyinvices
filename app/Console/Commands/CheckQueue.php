<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ProcessQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Queue;
class CheckQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    use DispatchesJobs;
    protected $signature = 'CheckQueue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $job   = (new ProcessQueue());

        $this->dispatch($job);
      
    }
}
