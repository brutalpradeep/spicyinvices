<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Queue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use App\Models;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Queue::after(function ($connection, $job, $data) {

            if(isset($data['data']['command'])){
                $data_all = unserialize($data['data']['command']);\Log::info(json_encode($data_all));
                if(isset($data_all->job_name) && $data_all->job_name=='orders') {
                    $config = \MainConfig::$config;
                    $shopName = $data_all->shop_name;
                    $GLOBALS['database'] = $config['DB']['DB_DATABASE'];
                    $shopUser = Models\ShopUsers::where('shop_name', '=', $shopName)->where('is_active',1)->update(['being_synced' => 0]);
                }


//                Mail::raw(json_encode( $data), function($message) {
//                    $message->to('brutalpradeep@gmail.com', 'Pradeep')->subject('Job Complete')->from('support@spicegems.com','Spicegems');
//                });
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
