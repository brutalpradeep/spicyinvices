<?php

namespace App\Models;

use App\Models\Connection;
class Orders extends Connection
{
    protected $table = 'shop_orders';
    public $primaryKey  = 'order_id';
}