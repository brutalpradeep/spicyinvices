<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Config;

class Connection extends Model
{
    public function __construct()
    {
        $database =   $GLOBALS['database'];
		$filename = base_path('app.yml');
        \MainConfig::readConfig($filename);

       $connection =  array('default' => $database,
           'connections'=>array($database => [
               'driver'    => 'mysql',
               'host'      => \MainConfig::$config['DB']['DB_HOST'],
               'database'  => $database,
               'username'  => \MainConfig::$config['DB']['DB_USERNAME'],
               'password'  => \MainConfig::$config['DB']['DB_PASSWORD'],
               'charset'   => 'utf8',
               'collation' => 'utf8_unicode_ci',
               'prefix'    => '',
               'strict'    => false,
           ]));
       Config::set('database', $connection);
       return $connection;
   }

   
}
