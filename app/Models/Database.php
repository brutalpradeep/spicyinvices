<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Database extends Model
{

    function __construct($db)
    {

        return DB::statement('CREATE DATABASE '. $db);
    }

}
