<?php

namespace App\Models;
use App\Models\Connection;

class ShopUsers extends Connection
{
    protected $table = 'shop_users';

    public static function getShopUserByShopName($shopName)
    {
        $shop = ShopUsers::where('shop_name',$shopName)->where('is_active',1)->orderBy('id','desc')->first();
        return $shop;
    }
}
