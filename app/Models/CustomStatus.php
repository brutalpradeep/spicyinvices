<?php

namespace App\Models;
use App\Models\Connection;
class CustomStatus extends Connection
{
    protected $table = 'shop_order_status';

    protected static function seedData(){

        CustomStatus::insert(array(
            array(
                'internal_name' => 'BackOrdered',
                'external_name' => 'BackOrdered',
                'color'=>'2ebb58',
                'is_email'=>1,
                'is_active'=>1,
                'email_message'=>'<p>HI [customer-name],<br /> <br />We are working hard to fulfill your order. Here are the details of your order.<br />Order Number: [order-number]<br />Current Status : [custom-status]<br /><br />Regards,<br />[shop-name]</p>',
                'email_subject'=>'Update on your [order-number]',
                'created_at' => date('Y-m-d G:i:s'),
                'updated_at' => date('Y-m-d G:i:s')
            ),

            array(
                'internal_name' => 'In Production',
                'external_name' => 'In Production',
                'color'=>'f02121',
                'is_email'=>1,
                'is_active'=>1,
                'email_message'=>'<p>HI [customer-name],<br /> <br />We are working hard to fulfill your order. Here are the details of your order.<br />Order Number: [order-number]<br />Current Status : [custom-status]<br /><br />Regards,<br />[shop-name]</p>',
                'email_subject'=>'Update on your [order-number]',
                'created_at' => date('Y-m-d G:i:s'),
                'updated_at' => date('Y-m-d G:i:s')
            ),
            
            array(
                'internal_name' => 'Packing',
                'external_name' => 'Packing',
                'color'=>'4a7bee',
                'is_email'=>1,
                'is_active'=>1,
                'email_message'=>'<p>HI [customer-name],<br /> <br />We are working hard to fulfill your order. Here are the details of your order.<br />Order Number: [order-number]<br />Current Status : [custom-status]<br /><br />Regards,<br />[shop-name]</p>',
                'email_subject'=>'Update on your [order-number]',
                'created_at' => date('Y-m-d G:i:s'),
                'updated_at' => date('Y-m-d G:i:s')
            )
        ));

    }
}
