<?php

namespace App\Jobs;

use App\Http\Controllers\Shopify\ServeController;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use DEFT\Shopify;

class WebhookOrder extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $orders;
    protected $db;
    protected $shop_id;
    public function __construct($orders,$db,$shop_id)
    {
        $this->orders = $orders;
        $this->db = $db;
        $this->shop_id = $shop_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $controller = new ServeController();
        $controller->readOrder($this->orders,$this->db ,$this->shop_id,true);
    }
}
