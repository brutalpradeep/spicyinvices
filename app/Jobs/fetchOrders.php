<?php

namespace App\Jobs;

use App\Http\Controllers\Shopify\ServeController;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use DEFT\Shopify;


class fetchOrders extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $db;
    public $shop_name;
    protected $shop_id;
    protected $count;
    protected $token;
    public $job_name='orders';
    public function __construct($db,$shop,$shop_id,$token,$count)
    {

        $this->db = $db;
        $this->shop_name = $shop;
        $this->shop_id = $shop_id;
        $this->token = $token;
        $this->count = $count;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

       // $shop = $this->shop;
	    $controller = new ServeController();
		
		
        $caller = new Shopify\ShopifyDialer();
        $caller->setAccessToken($this->token);
        $caller->setShopName($this->shop_name);
        $orderHelper=new Shopify\OrderHelper($caller);
		$shopDetail = $controller->getShop($this->shop_name);
        $page = ceil($this->count/250);

        if($page && $shopDetail){
            for($i=1;$i<=$page;$i++){
                try{
                $result=$orderHelper->getOrders(array('status'=>'open','limit'=>250,'page'=>$i));
				$headers = $result->headers;
                if($headers['Status-Code'] == 401)
                {
                    \Log::error('Unauthorized');
                    break;
                }
                $orders = json_decode($result->body);
              
                if(isset($orders->orders)){
                    $controller->readOrder($orders,$this->db ,$this->shop_id);
                }



                 }catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        }
    }
    }
}
