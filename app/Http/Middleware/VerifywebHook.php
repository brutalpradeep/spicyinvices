<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Helpers\Log\LogFile;
class VerifywebHook
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $inputs = file_get_contents('php://input');
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $calculated_hmac = base64_encode(hash_hmac('sha256', $inputs,\MainConfig::$config['Shopify']['SHOP_SECRET'], true));
        if($hmac_header != $calculated_hmac);
        {
            $file = new LogFile();
            $file->Error('Invalid Request');
        }
        return $next($request);
    }
}
