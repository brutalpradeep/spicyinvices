<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;

class CheckBrowser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $agent = new Agent();
        if($agent->browser()=='Safari')
        {
            $session = \Session::get('shop_name');
            if($session=='')
            {
                ?>
                <script>
                    location.href = '<?php echo \URL::to('/shopify/user-login'); ?>';
                </script>
            <?php
            }
        }

        return $next($request);
    }
}
