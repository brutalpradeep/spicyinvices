<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */

    public function handle($request, Closure $next)
    {
        if ($this->isReading($request) || $this->excludedRoutes($request) || $this->tokensMatch($request))
        {
            return $this->addCookieToResponse($request, $next($request));
        }
        throw new TokenMismatchException;
    }


    protected function excludedRoutes($request)
    {

        $routes = ['shopify/uninstall','index','shopify/oauth','shopify/webhook','hooks/{type}/{event}','shopify/orders/status','shopify/orders/status/edit','shopify/orders/status/delete','shopify/orders/status/disable','shopify/orders/status/get'];


        foreach($routes as $route)
            if ($request->is($route))
                return true;

        return false;
    }
}
