<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Http\Request;

class HttpsProtocol
{


    protected $app;
    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->app->environment() === 'prod') {
            // for Proxies
            Request::setTrustedProxies([$request->getClientIp()]);
            if (!isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
                return redirect()->secure($request->path());
            }
        }
        return $next($request);
    }
}
