<?php
namespace DEFT\Shopify;
use DEFT\Shopify\Api;

use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpFoundation\RedirectResponse as HttpRedirectResponse;
class ShopifyDialer extends AbstractShopifyCaller
{
    protected $SHOP_URI = 'https://%s.myshopify.com';
    protected $AUTHORIZATION_URI = 'https://%s.myshopify.com/admin/oauth/authorize';
    
    protected $shopName;
    protected $accessToken;


    public function getShopUri()
    {
        return sprintf($this->SHOP_URI, $this->getShopName());
    }

    protected function getShopName()
    {
        return $this->shopName;
    }

    public function setShopName($shopName)
    {
        $this->shopName = $shopName;
    }

    public function getAccessToken($parameters = '')
    {
        $response = $this->send('/admin/oauth/access_token',$parameters, 'post');
        $token  = json_decode($response->body);
		if(isset($token->access_token))	
        return $token->access_token;
		
		
    }
	
	public function redirectToShop($vars = '')
    {
        $url =  sprintf($this->AUTHORIZATION_URI, $this->getShopName());
        if (!empty($vars)) {
            $url .= (stripos($url, '?') !== false) ? '&' : '?';
            $url .= (is_string($vars)) ? $vars : http_build_query($vars, '', '&');
        }
        return $url;
    }
	
	
	public function initiateInstall($vars = '')
		{
			$url =  sprintf($this->AUTHORIZATION_URI, $this->getShopName());
			if (!empty($vars)) {
				$url .= (stripos($url, '?') !== false) ? '&' : '?';
				$url .= (is_string($vars)) ? $vars : http_build_query($vars, '', '&');
			}
          
			$response =  HttpRedirectResponse::create(($url));
			$response->send();
			exit;
		 //   return redirect()->away($url);


		}
    public function setAccessToken($access_token = '')
    {
        $this->accessToken = $access_token;
    }

    //Write codes here for CUrl
    // $method contains either get,post,delete,put (get to retrieve, post to create new, delete to delete, put to update)
    protected function send($url, $parameters, $method)
    {

        $curl = new Api();
        $url  = $this->getShopUri().$url;

        $curl->options['CURLOPT_SSL_VERIFYHOST'] = false;
        $curl->options['CURLOPT_SSL_VERIFYPEER'] = false;
        if($this->accessToken){
            $curl->headers['X-Shopify-Access-Token'] = $this->accessToken;
        }
        $response = false;

        $response =$curl->request(strtoupper($method),$url, $parameters);

        return $response;
    }
}