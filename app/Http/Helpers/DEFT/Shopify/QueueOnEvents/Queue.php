<?php
namespace DEFT\Shopify\QueueOnEvents;

use DEFT\Shopify\HelperInterface;
/**
 * Class Queue
 * usage
 * $caller = new ShopifyDialer('some_token');
 * $a = new Installed;
 * $customer = new CustomerHelper($caller);
 * $scriptTag = new ScriptTagHelper($caller);
 * $scriptTag->delete($scriptId);
 * $a->pushQueue($scriptTag)->delete($scriptId);
 * $customer->delete(1);
 * $a->pushQueue($customer)->get($scriptId);
 */

abstract class Queue
{
    protected $caller;
    protected $queue;


    public function pushQueue(HelperInterface $object)
    {
        static $id = 0;

        $virtualQueue = new VirtualQueue($object);
        $this->queue[$id] = $virtualQueue;
        $id++;
        return $virtualQueue;

    }

    public function executeQueue()
    {
        $data=array();
        foreach ($this->queue as $object) {
            /* @var $object VirtualQueue */
            $data[]= $object->executeQueue();
        }
        return $data;
    }

}

