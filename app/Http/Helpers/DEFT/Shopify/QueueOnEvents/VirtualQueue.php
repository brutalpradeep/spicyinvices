<?php
namespace DEFT\Shopify\QueueOnEvents;
class VirtualQueue
{

    private $object;
    private $operation = array();

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function executeQueue()
    {
      if(!isset($this->operation['arguments'][0])){
          $this->operation['arguments'][0]=array();
        }
        return call_user_func(array($this->object, $this->operation['name']), $this->operation['arguments'][0]);
    }

    function __call($name, $arguments)
    {
       // if (is_callable($this->object, $name)) {
            $this->operation = array(
                'name' => $name,
                'arguments' => $arguments
            );
       // }
    }

}