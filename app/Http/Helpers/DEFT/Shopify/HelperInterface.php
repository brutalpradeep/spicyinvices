<?php
namespace DEFT\Shopify;
use DEFT\Shopify\AbstractShopifyCaller;
interface HelperInterface {

    public function __construct(AbstractShopifyCaller $caller);
}