<?php

namespace DEFT\Shopify;


class ValidateHelper {

    protected $sharedSecret;

    public function setClientSecret($secret)
    {
        $this->sharedSecret = $secret;
    }

    protected function getClientSecret()
    {
        return $this->sharedSecret;
    }


    public function isValidRequest(array $params)
    {

        $this->assertRequestParamIsNotNull(
            $params, 'timestamp', 'Expected timestamp in query params'
        );

        $requestTimestamp = $params['timestamp'];
        $secondsPerDay = 24 * 60 * 60;
        $olderThanOneDay = $requestTimestamp < (time() - $secondsPerDay);

        return ($olderThanOneDay) ? false : $this->validateSignature($params);

    }

    protected function assertRequestParamIsNotNull(
        array $params, $key, $message
    ) {

        $value = array_key_exists($key, $params)
            ? $params[$key] : null;

        if (is_null($value)) {
            $e = new RequestException($message);
            $e->setQueryParams($params);
            throw $e;
        }

    }

    public function validateSignature(array $request)
    {

        $this->assertRequestParamIsNotNull(
            $request, 'hmac', 'Expected signature in query params'
        );

        $hmac = $request['hmac'];

        return $this->generateSignature($request) === $hmac;

    }

    public function generateSignature(array $request)
    {

        $params = $request;

        // The signature and hmac entries are removed from the map, leaving the
        // remaining parameters.
        unset($params['signature']);
        unset($params['hmac']);

        // Each key is concatenated with its value, seperated by an = character,
        // to create a list of strings
        $collected = array_map(function($key, $value) {
            return $key . "=" . $value;
        }, array_keys($params), $params);

        // The list of key-value pairs is sorted lexicographically
        sort($collected);

        // and concatenated together with & to create a single string
        $collected = implode('&', $collected);

        // this string processed through an HMAC-SHA256 using the Shared Secret
        // as the key
        return hash_hmac('sha256', $collected, $this->getClientSecret());

    }

}