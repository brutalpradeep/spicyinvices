<?php

namespace DEFT\Shopify;


class ShopHelper extends AbstractHelper{

    public static function getNameFromUrl($urlString)
    {
        if (strpos($urlString,'.myshopify.com') !== false) {
            $urlString = explode('.myshopify.com', $urlString);
            $strName =str_replace(array('http://','https://'),'',$urlString[0]);
        }
        else{
            $strName = $urlString;
        }
        return $strName;
    }

}