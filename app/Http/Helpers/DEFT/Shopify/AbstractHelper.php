<?php
namespace DEFT\Shopify;
use DEFT\Shopify\HelperInterface;
abstract class AbstractHelper implements HelperInterface
{

    protected $caller;
	protected $id;
    private $methods = array(
        'get'=>'get',
        'create'=>'post',
        'update'=>'put',
        'delete'=>'delete',
        'count'=>'get',
        'close'=>'post',
        'open'=>'post',
        'cancel'=>'post',
        'activate'=>'post'
    );

    public function __construct( AbstractShopifyCaller $caller)
    {
        $this->caller=$caller;
    }
    function __call($name, $arguments)
    {
        $classNamespace = explode('\\',get_called_class());
        $class = end($classNamespace);
        $filename = __DIR__.'/Api/'. $class.'.yml';
        \MainConfig::readConfig($filename);
        $allMethods = \MainConfig::$config;

        foreach($this->methods as $key=>$value)
        {

            if (strpos($name,$key) !== false) {

                $methodName = $value;
                break;
            }
        }

        $method = $allMethods[$name];
        $url = $method['url'];

        if (!isset($arguments[0])) {  $arguments[0] = false;}

        $id=$this->getId();

        if(isset($id) && strpos($url,'{id}')!==false){
            $url = str_replace('{id}',$id,$url);

        }
        return $this->caller->$methodName($url, $arguments[0]);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    protected function getId()
    {
        return $this->id;
    }
}

