<?php
namespace DEFT\Shopify;
/**
 * @method post($url, $arguments)
 * @method get($url, $arguments)
 * @method delete($url, $arguments)
 * @method update($url, $arguments)
 * Class AbstractShopifyCaller
 */
abstract class AbstractShopifyCaller
{
    private $methods = array('post', 'get', 'delete', 'put');
    protected $token;

    /**
     * send will be used to defined different libraries.
     * @return mixed
     */
    abstract protected function send($url, $parameters, $method);

    public function __contruct($token)
    {

        if (!$token) {
            throw new \Exception();
        }
        $this->token = $token;
    }


    function __call($name, $arguments)
    {
        if (!isset($arguments[1])) {
            $arguments[1] = false;
        }
        if (in_array($name, $this->methods)) {
            return $this->send($arguments[0], $arguments[1], $name);
        }

    }

}

