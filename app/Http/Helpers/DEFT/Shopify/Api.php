<?php
/**
 * Created by PhpStorm.
 * User: Naresh
 * Date: 9/2/2015
 * Time: 2:01 PM
 */
namespace DEFT\Shopify;
require_once base_path('vendor/shuber/curl/curl.php');
class Api extends \Curl{

    public function request($method, $url, $vars = array(), $enctype = null) {

        if($method=="GET"){
            if (!empty($vars)) {
                $url .= (stripos($url, '?') !== false) ? '&' : '?';
                $url .= (is_string($vars)) ? $vars : http_build_query($vars, '', '&');
            }
            $vars = '';
        }
        $this->request = curl_init();
        if (is_array($vars) && $enctype != 'multipart/form-data') {
            $vars = http_build_query($vars, '', '&');
            $vars = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $vars);
        }

        $this->set_request_method($method);
        $this->set_request_options($url, $vars);
        $this->set_request_headers();

        $response = curl_exec($this->request);
        if (!$response) {
            throw new \CurlException(curl_error($this->request), curl_errno($this->request));
        }

        $response = new \CurlResponse($response);

        curl_close($this->request);

        return $response;
    }
}