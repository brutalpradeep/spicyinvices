<?php

namespace App\Http\Controllers\Shopify;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Log\LogFile;
use DEFT\Shopify;
use App\Models;
use App\Models\Database;
use Illuminate\Support\Facades\Session;
use Log;
use App\Lib;
use Illuminate\Support\Facades\Config;
use Mockery\CountValidator\Exception;
use App\Jobs\WebhookOrder;

class WebHookController extends ServeController{

    protected $config;
    function __construct()
    {
        $this->config =  \MainConfig::$config;
        $this->middleware('CheckBrowser',['except'=>['getShopData','orderDetails','doOrderDetails','MACUserLogin']]);
    }
    /* Accept All Webhook Response */
    public function get_WebHooks_Request($type,$event)
    {
        $this->middleware('verify_webhook');
        $route = $type.'_'.$event;
        $this->$route();
    }
    /* Call When App Uninstalled */
    public function app_uninstalled(){
        try {

            $mes  = \Input::all();
            $shop =$this->getShop($mes['myshopify_domain']);

            if($shop) {
                $caller = new Shopify\ShopifyDialer();
                $caller->setAccessToken($shop->permanent_token);
                $caller->setShopName($shop->shop_name);
                $rac = new Shopify\RACHelper($caller);
                $res = $rac->getRAC();
                $res = json_decode($res);

                if(isset($res->errors)) {

                    $db = $shop->database_name;
                    $shop->is_active = 0;
                    $shop->save();

                    if($shop->payment_status !='pending'){
                        $GLOBALS['database'] =$db;
                        $email_to = Models\Settings::where('entity_name','email_from')->select('entity_value')->first();
                        $database =  $this->config['DB']['DB_DATABASE'];

                        $connection =  array('default' => env('DB_CONNECTION', $database),
                            'connections'=>array($database => [
                                'driver'    => 'mysql',
                                'host'      => $this->config['DB']['DB_HOST'],
                                'database'  => $database,
                                'username'  => $this->config['DB']['DB_USERNAME'],
                                'password'  => $this->config['DB']['DB_PASSWORD'],
                                'charset'   => 'utf8',
                                'collation' => 'utf8_unicode_ci',
                                'prefix'    => '',
                                'strict'    => false,
                            ]));
                        Config::set('database', $connection);



                        $data_object['view'] ='emails.uninstall';
                        $data_object['data']=array('email'=>$email_to->entity_value,'shop_name'=>$shop->shop_name,'date'=>date('Y-m-d G:i:s'));
                        $email_object['to']='support@spicegems.com';

                        $email_object['name']='';
                        $email_object['from_email']= $this->config['MAIL']['EMAIL_FROM'];
                        $email_object['from_name']= $this->config['MAIL']['EMAIL_FROM_NAME'];
                        $email_object['subject'] ='Email Subject - App Uninstall '.$shop->shop_name;
                        $email = new Lib\Email();
                        $result = $email->queueEmail($data_object,$email_object);
                    }
                }
            }
        }
        catch(Exception $e){
            Log::info($e->getMessage());
        }
    }

    /* Call When App Orders Create/Updated */
    public function orders_create(){
        try
        {
            $shop = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
            $shop_detail = $this->getShop($shop);
			if($shop_detail){

				$order['orders'][] =(object) \Input::all();
				$data = (object)$order;
                $database =  $this->config['DB']['DB_DATABASE'];

                $connection =  array('default' => env('DB_CONNECTION', $database),
                    'connections'=>array($database => [
                        'driver'    => 'mysql',
                        'host'      => $this->config['DB']['DB_HOST'],
                        'database'  => $database,
                        'username'  => $this->config['DB']['DB_USERNAME'],
                        'password'  => $this->config['DB']['DB_PASSWORD'],
                        'charset'   => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                        'prefix'    => '',
                        'strict'    => false,
                    ]));
                Config::set('database', $connection);

                $job = (new WebhookOrder($data,$shop_detail->database_name,1))->onQueue('high');

                $this->dispatch($job);
				//$this->updateWebHookOrder($data,$shop_detail->database_name);

			}
        }
        catch(Exception $e){
            Log::info($e->getMessage());
        }
    }

    public function customers_update(){
        try
        {
            $shop = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
            $shop_detail = $this->getShop($shop);
            if($shop_detail){

                $customers[] =(object) \Input::all();
                $data = (object)$customers;
                $this->updateOrCreateCustomer($data,$shop_detail->database_name,'',1);


            }
        }
        catch(Exception $e){
            Log::info($e->getMessage());
        }
    }

    public function customers_create(){
        $this->customers_update();
    }

    public function orders_updated(){

        $this->orders_create();
    }
    /* Call When App Orders Deleted */
    public function orders_delete(){
        try
        {
            $data =(object) \Input::all();
            $shop = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
            $shop_detail = $this->getShop($shop);
            if($shop_detail){

                $GLOBALS['database']=$shop_detail->database_name;
                Models\Product::where('order_id',$data->id)->delete();
                Models\Orders::where('order_id',$data->id)->delete();

            }
        }
        catch(Exception $e){
            Log::info($e->getMessage());
        }
    }

    public function allHooks(){

        $shopName=\Session::get('shop_name');
        $token=\Session::get('permanent_token');
        $caller = new Shopify\ShopifyDialer();
        $caller->setShopName($shopName);
        $caller->setAccessToken($token);
        $tag  = new Shopify\WebhookHelper($caller);

        $allTag = $tag->getWebHooks();
        echo '<pre>';
        print_r($allTag);exit;

    }
}