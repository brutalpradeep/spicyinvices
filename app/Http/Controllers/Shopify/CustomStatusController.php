<?php

namespace App\Http\Controllers\Shopify;
use App\Http\Controllers\Controller;
use App\Models;
use DEFT\Shopify;

class CustomStatusController extends Controller {

	protected $layout = 'layouts.defaults';
	protected $config;
    public function __construct()
    {
		 $this->middleware('select_db');
        $this->middleware('CheckBrowser',['except'=>['getShopData','orderDetails','doOrderDetails','MACUserLogin']]);
		 $this->config =  \MainConfig::$config;
        $this->get_user_data();
    }
	/* Show Custom Order Status */
    public function CustomOrderStatus()
    {
		$count = Models\CustomStatus::where('is_delete',0)->count();
		$action = \Input::get('action');
		if(!isset($action))
		{
			$order_status = Models\CustomStatus::where('is_delete',0)->take(11)->get();
			return view($this->layout, ['content' => view('shopify.order_status',array('order_status'=>$order_status,'status_count'=>$count))])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);
		}
		else{
			$skip = 11;
			if($count <= $skip){	return '';}
			$limit = $count - $skip; 
			$order_status = Models\CustomStatus::where('is_delete',0)->skip($skip)->take($limit)->get();
			return json_encode($order_status);
		}
    }
	/* Edit Custom Orders Status */
    public function CustomOrderStatusEdit()
    {
        $id = \Input::get('id');
        $order_status = Models\CustomStatus::find($id);
        return json_encode($order_status);
    }
	/* Update Custom Orders Status */
    public function getCustomOrderStatus()
    {
        $input = \Input::all();

        $input['color'] = str_replace('#','',$input['color']);
        if($input['order_status_action']=='update')
        {
            $orderStatus = Models\CustomStatus::find($input['order_status_id']);
        }else
        {
            $orderStatus = new Models\CustomStatus();
            $orderStatus->is_active = 1;
        }

        $orderStatus->internal_name = $input['internal_name'];
        $orderStatus->external_name = $input['external_name'];
        $orderStatus->color = $input['color'];
        $orderStatus->is_email = (isset($input['is_email']))?1:0;
        $orderStatus->is_delete = 0;
        $orderStatus->email_message = $input['email_message'];
        $orderStatus->email_subject = $input['email_subject'];

        $orderStatus->is_email_department = (isset($input['is_email_department']))?1:0;
        $orderStatus->department_email = (isset($input['department_email']))?$input['department_email']:'';
        $orderStatus->department_email_message = $input['department_email_message'];
        $orderStatus->department_email_subject = (isset($input['department_email_subject']))?$input['department_email_subject']:'';

        $orderStatus->save();
		
		return response()->json($orderStatus);
    }
	/* Delete Custom Orders Status */
    public function CustomOrderStatusDelete()
    {
        $ids = \Input::get('ids');
        Models\CustomStatus::whereIn('id',$ids)->update(array('is_delete' => 1));
        Models\Orders::whereIn('custom_status_id',$ids)->update(array('custom_status_id' => 0));

    }
	/* Enable/Disable Custom Orders Status */
    public function CustomOrderStatusDisable()
    {
		$inputs = \Input::all();
		
        Models\Orders::where('custom_status_id',$inputs['pk'])->update(array('custom_status_id' => $inputs['value']));
        Models\CustomStatus::where('id',$inputs['pk'])->update(array('is_active' => $inputs['value']));
		$order_status = Models\CustomStatus::where('id',$inputs['pk'])->get();
        return json_encode($order_status);
    }
	
	//UPDATE ORDER TAGS ON DELETE/DISABLE CUSTOM STATUS
	public function updateTags($ids){
        $all_status = Models\CustomStatus::where('is_delete',0)->get();
        $token=\Session::get('permanent_token');
        $shop=\Session::get('shop_name');

        $caller = new Shopify\ShopifyDialer();
        $caller->setAccessToken($token);
        $caller->setShopName($shop);

        $queue = new Shopify\QueueOnEvents\Installed;


        $all_orders =Models\Orders::whereIn('custom_status_id',$ids)->get();
        foreach($all_orders as $order){
            $order_detail = json_decode($order->data);
            $tags_array = array();
            $tags_array =array_map('trim',  explode(',',$order_detail->tags));
            foreach($all_status as $sta){
				$custom_status =  'spicegems-'.$sta->external_name;
                if(in_array($custom_status,$tags_array)){
                    $tags_array = array_diff($tags_array, array($custom_status));
                }
            }


            $data=array(
                "order"=>array(
                    'id'=>$order_detail->id,
                    'tags'=>implode(',',$tags_array)
                )
            );
            $orderHelper=new Shopify\OrderHelper($caller);
            $orderHelper->setId($order_detail->id);

            $queue->pushQueue($orderHelper)->updateOrderById($data);
        }
        $result= $queue->executeQueue();
        return true;
    }
	
}