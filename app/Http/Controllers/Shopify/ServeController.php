<?php

namespace App\Http\Controllers\Shopify;

use Illuminate\Database\Eloquent\Model;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Input\Input;
use DEFT\Shopify;
use App\Models;
use App\Lib;
use App\Jobs\fetchOrders;
use Illuminate\Support\Facades\Config;
use App\Console\Commands\CreateDb;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
class ServeController extends Controller
{
    protected $layout = 'layouts.defaults';
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    private $placeholders =  array('[order-number]'=>'name',
        '[custom-status]'=>'custom_status_id',
        '[shopify-order-status]'=>'status',
        '[customer-name]'=>'customer_name'
    );
    public function __construct()
    {

       $this->middleware('select_db',['except'=>['index','oauth']]);
       $this->config =  \MainConfig::$config;
      // $this->getOrders();
        $this->middleware('CheckBrowser',['except'=>['getShopData','orderDetails','doOrderDetails','MACUserLogin']]);
        $this->get_user_data();
    }
     public function getShop($shop)
    {

        if (strpos($shop,'.myshopify.com') !== false) {
            $shop = explode('.myshopify.com', $shop);
            $shopName =str_replace(array('http://','https://'),'',$shop[0]);
        }
        else{
            $shopName = $shop;
        }

        $GLOBALS['database'] = $this->config['DB']['DB_DATABASE'];
       
        $shop = Models\ShopUsers::where('shop_name',$shopName)->where('is_active',1)->orderBy('id','desc')->first();
        if(isset($shop->database_name))
        {
            $GLOBALS['database'] = $shop->database_name;
        }

        return $shop;
    }

    public function getShopByUser($shop)
    {
        $shop = explode('.myshopify.com', $shop);
        $shopName =str_replace(array('http://','https://'),'',$shop[0]);
        $shop = Models\Shop::where('shop_name',$shopName)->orderBy('id','desc')->first();
        return $shop;
    }
	/* Show App Main Page */
    public function index(){
	
        $inputs = \Input::all();
        $caller = new Shopify\ShopifyDialer();
        $shop = explode('.myshopify.com', $inputs['shop']);
        $shopName = $shop[0];
        $shop =$this->getShop( $inputs['shop']);

        $dta = array('client_id' => $this->config['Shopify']['SHOP_CLIENTID'], 'client_secret' => $this->config['Shopify']['SHOP_SECRET'],'scope'=> $this->config['Shopify']['SCOPE'], 'redirect_uri' => \URL::to('/shopify/oauth') );
        $caller->setShopName($shopName);


        if($shop){
            if($shop->is_active==1){


                if($shop->payment_status!='accepted' || floatval($this->config['App']['VERSION']) > floatval($shop->version) || $shop->new_hooks !='' )
                {
                    $url = $caller->redirectToShop($dta);
                    return view('shopify/RACRedirect',array('url'=>$url));
                }

                \Session::set('database_name',$shop->database_name);
                \Session::set('permanent_token',$shop->permanent_token);
                \Session::set('shop_name',$shop->shop_name);
                $GLOBALS['database'] = $shop->database_name;
                return redirect()->to('shopify/home');
            }
        }

        $caller->initiateInstall($dta);


    }
	
	public function home()
    {
	   $inputs = \Input::all(); 
	   if(isset($inputs['shop'])){
		  $shop = explode('.myshopify.com', $inputs['shop']);
		  $shopName = $shop[0];
		  $shop =$this->getShop( $inputs['shop']);	
		  \Session::set('database_name',$shop->database_name);
		  \Session::set('permanent_token',$shop->permanent_token);
		  \Session::set('shop_name',$shop->shop_name);
		  $GLOBALS['database'] = $shop->database_name;
		}				
	   return view($this->layout, ['content' => view('shopify/home')])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);
    }
	
	
	/* Authorize App Request */
    public function oauth(){

        $valid=new Shopify\ValidateHelper();
        $valid->setClientSecret($this->config['Shopify']['SHOP_SECRET']);
        if($valid->isValidRequest(\Input::all()))
        {
            $code = \Input::get('code');
            $shopDomain = \Input::get('shop');

            $shop = explode('.myshopify.com', $shopDomain);
            $shopName = $shop[0];

            $caller = new Shopify\ShopifyDialer();
            $caller->setShopName($shopName);

            $dta = array('client_id' => $this->config['Shopify']['SHOP_CLIENTID'], 'client_secret' => $this->config['Shopify']['SHOP_SECRET'], 'code'=>$code);
			$token = $caller->getAccessToken($dta);

            $permanentAccessToken =  ($token !='')?$token:'';
            $caller->setAccessToken($permanentAccessToken);
            if($permanentAccessToken)
            {
                \Session::set('permanent_token',$permanentAccessToken);
                \Session::set('shop_name',$shopName);
                $hooks = new Lib\WebHooks($shopName,$permanentAccessToken);
                $hooks->setAddress(\URL::to('/hooks'));
              //  $hooks->setAddress('https://aca6781d.ngrok.io/laravel/public/hooks');

                $shop = $this->getShop($shopName);

                if( $shop){


                    if((float)$this->config['App']['VERSION'] > (float)$shop->version ){

                        $shop->version = $this->config['App']['VERSION'];

                        if($shop->new_hooks){

                            $new_hooks = array_filter(explode(',',$shop->new_hooks));
                            foreach($new_hooks as $hook){
                                $hooks->createWebHook($hook);
                            }
							$hooks->executeWebHook();
                        }
                        

                        $shop->new_hooks = '';
                        $shop->save();

                        \Session::set('database_name',$shop->database_name);
                        $GLOBALS['database'] = $shop->database_name;
                        $shopByUser = $this->getShopByUser($shopName);

                        $shopByUser->version = $this->config['App']['VERSION'];

                        $shopByUser->save();
                       return redirect()->to('/shopify/home');
                    }

                }

                if(!$shop){
                    $shop  =  Models\ShopUsers::where('shop_name','=','' )->where('permanent_token','=','' )->first();
                    if(!$shop){
                        $databasename ='cos_'. md5(uniqid(rand(), true));

                        $job = (new CreateDb($databasename));

                        $this->dispatch($job);

                        $shop = Models\ShopUsers::where('database_name',$databasename )->first();
                    }
                }
                $shop->shop_name = $shopName;
                $shop->permanent_token = $permanentAccessToken;
                $shop->is_active = 1;
                $shop->payment_status = 'pending';
                $shop->version = $this->config['App']['VERSION'];
                $shop->save();

                \Session::set('database_name',$shop->database_name);
                $GLOBALS['database'] = $shop->database_name;
                $shopByUser = $this->getShopByUser($shopName);
                if(!$shopByUser){
                    $shopByUser  =new Models\Shop();
                }
                $shopByUser->shop_name = $shopName;
                $shopByUser->permanent_token = $permanentAccessToken;
                $shopByUser->is_active = 1;
                $shopByUser->payment_status = 'pending';
                $shopByUser->database_name = $shop->database_name;
                $shopByUser->version = $this->config['App']['VERSION'];
                $shopByUser->save();



                $hooks->createWebHook('app/uninstalled');
                $res = $hooks->executeWebHook();

                $is_test= $this->config['Plan']['TEST'];
                if(isset($this->config['TestStore']))
                {
                    $test_store =explode(',',$this->config['TestStore']);
                    if(in_array($shopName,$test_store))
                    {
                        $is_test=TRUE;
                    }
                }

                $dta = array(
                    'recurring_application_charge' => array(
                        "name" => $this->config['Plan']['PLAN_NAME'],
                        "price" => $this->config['Plan']['PRICE'],
                        "return_url" => \URL::to('/shopify/activate'),
                        "test" =>$is_test,
                        "trial_days"=>$this->config['Plan']['TRIAL_DAYS']
                    )
                );
                $rac = new Shopify\RACHelper($caller);
                $res = $rac->createRAC($dta);
                $res = json_decode($res->body);
                $res = $res->recurring_application_charge;
                $confirm_url = $res->confirmation_url;

                return redirect()->to($confirm_url);
            }
        }
        else
        {
            throw new Exception("invalid request");
        }
        return redirect()->to('/shopify/orders');
    }

    //GET ORDERS
    function getOrders()
    {
        $token=\Session::get('permanent_token');
        $shop=\Session::get('shop_name');

        $caller = new Shopify\ShopifyDialer();
        $caller->setAccessToken($token);
        $caller->setShopName($shop);

        $orderHelper=new Shopify\OrderHelper($caller);

        $countOrders=$orderHelper->countOrders();
        $count = json_decode($countOrders->body);
        $db  =\Session::get('database_name');
        $page = ceil($count->count/$this->config['Shopify']['DEFAULT_ORDER_COUNT']);
        $GLOBALS['database']=$db;
        $shopdata = $this->getShopByUser($shop);
        if($page){
            $result=$orderHelper->getOrders(array('status'=>'open','limit'=>$this->config['Shopify']['DEFAULT_ORDER_COUNT'],'page'=>1));
            $orders = json_decode($result->body);
           // echo '<pre>';print_r($orders);exit;
            $this->readOrder($orders,$db,$shopdata->id);
        }
        if($page>1){

            $database =  $this->config['DB']['DB_DATABASE'];

            $connection =  array('default' => env('DB_CONNECTION', $database),
                'connections'=>array($database => [
                    'driver'    => 'mysql',
                    'host'      => $this->config['DB']['DB_HOST'],
                    'database'  => $database,
                    'username'  => $this->config['DB']['DB_USERNAME'],
                    'password'  => $this->config['DB']['DB_PASSWORD'],
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                    'strict'    => false,
                ]));
            Config::set('database', $connection);

            $job = (new fetchOrders($db,$shop,$shopdata->id,$token,$count->count));

            $this->dispatch($job);

            $GLOBALS['database'] = $this->config['DB']['DB_DATABASE'];
            $shopName = Shopify\ShopHelper::getNameFromUrl($shop);
            $shopUser = Models\ShopUsers::where('shop_name', '=', $shopName)->where('is_active',1)->update(['being_synced' => 1]);
        }
        return true;
    }

    public function updateWebHookOrder($orders,$db='',$shop_id=''){
        $this->readOrder($orders,$db,$shop_id,true);

    }
	
	/* Save Orders/Products/Customers */
    public function readOrder($orders,$db='',$shop_id='',$hook_update = false){

        $GLOBALS['database'] = $db;
        $orderClass = new Lib\UpdateOrder($orders,$shop_id,$hook_update);
        $orders = $orderClass->createOrUpdateOrders();
        return true;

    }

    public function updateOrCreateCustomer($customers,$db='',$shop_id='',$hook_update = false){

        $GLOBALS['database'] = $db;
        $customerClass = new Lib\UpdateCustomer($customers,$shop_id,$hook_update);
        $orders = $customerClass->updateAllCustomers();
        return true;

    }

    public function updateApp(){
        $shopName =\Session::get('shop_name');
        $shop =$this->getShop( $shopName);
        $url = '';

        if($shop){

            if(floatval($this->config['App']['VERSION']) > floatval($shop->version) || $shop->new_hooks !='')
            {

                $caller = new Shopify\ShopifyDialer();
                $dta = array('client_id' => $this->config['Shopify']['SHOP_CLIENTID'], 'client_secret' => $this->config['Shopify']['SHOP_SECRET'],'scope'=> $this->config['Shopify']['SCOPE'], 'redirect_uri' => \URL::to('/shopify/oauth') );
                $caller->setShopName($shopName);
                $url = $caller->redirectToShop($dta);
                return $url;
            }
        }
    }

    //Show all orders 
    public function showOrders(){
        $url   = $this->updateApp();
        if($url){
           return view('shopify/RACRedirect',array('url'=> $url));
        }

		$GLOBALS['database'] = \Session::get('database_name');
        $all_status  = Models\CustomStatus::where('is_active',1)->where('is_delete',0)->get();
		
        $a_status=array();
        $filter_status = array();
        $custom_status = array();
        foreach($all_status as $status){
            $data = array();
            $data['id']=$status->id;
            $data['text']=$status->external_name;
            $custom_status[] = array('id'=>$status->id,'label'=>$status->external_name);
            $a_status[] = $data;
        }
        $count_orders =Models\Orders::count();

        $payment_status = json_encode(array(
            array('id'=>'pending','label'=>'pending'),
            array('id'=>'authorized','label'=>'authorized'),
            array('id'=>'partially_paid','label'=>'partially_paid'),
            array('id'=>'paid','label'=>'paid'),
            array('id'=>'partially_refunded','label'=>'partially_refunded'),
            array('id'=>'refunded','label'=>'refunded'),
            array('id'=>'voided','label'=>'voided'),
            array('id'=>'any','label'=>'any'),
            array('id'=>'unpaid','label'=>'unpaid')
        ));
        $fulfillment_status = json_encode(array(
            array('id'=>'fulfilled','label'=>'fulfilled'),
            array('id'=>'','label'=>'unfulfilled'),
            array('id'=>'partial','label'=>'partial'),

        ));

        $order_status = json_encode(array(
            array('id'=>'open','label'=>'open'),
            array('id'=>'closed','label'=>'closed'),
            array('id'=>'cancelled','label'=>'cancelled'),
            array('id'=>'any','label'=>'any'),
        ));

        $filters = array('custom_status'=>json_encode($custom_status),'payment_status'=>$payment_status,'fulfillment_status'=>$fulfillment_status,'order_status'=>$order_status);

        return view($this->layout, ['content' => view('shopify/orders',array('filters'=>$filters,'all_status'=>json_encode($a_status),'total_orders'=>$count_orders))])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);

    }

    function get_brightness($hex){
        $c_r = hexdec(substr($hex, 0, 2));
        $c_g = hexdec(substr($hex, 2, 2));
        $c_b = hexdec(substr($hex, 4, 2));

        return (($c_r * 299) + ($c_g * 587) + ($c_b * 114)) / 1000;
    }
	/* Update Orders Custom Status */
    public function updateOrder(){
        $inputs = \Input::all();
        $id = $inputs['pk'];
        $column = $inputs['name'];
        $value=$inputs['value'];
        $order = Models\Orders::find($id);
        $order->$column=$value;
        $order->save();
        $status = Models\CustomStatus::find($order->custom_status_id);
        
        if($column=='custom_status_id' && $order->email !='' && $status->is_email ==1){
            $notify = $this->sendNotification($order,$status,false);
        }
        if($column=='custom_status_id' && $status->department_email !='' && $status->is_email_department ==1){
            $all_email = array_map('trim',explode(',',$status->department_email));
            $valid_email = array();
            foreach ( $all_email as $key => $email )
            {
                if(filter_var($email, FILTER_VALIDATE_EMAIL) != false){
                    // email is invalid, do what you want
                    $valid_email[] =$email;
                }
                // now email is valid
                // code for sending email
            }
            if($valid_email){
                $notify =  $this->sendNotification($order,$status,$valid_email);
            }
		}
        if (isset($status->color) && $this->get_brightness(  $status->color ) > 130) // will have to experiment with this number
            $color = '000000';
        else
            $color= 'ffffff';

        $status_color='';
        if(isset($status->color))
        {
            $status_color = $status->color;
        }
			 return response()->json(array('background'=>$status_color,'color'=>$color,'status_id'=>$order->custom_status_id));
       
        
    }


    //SEND NOTIFICATION FOR STATUS CHANGE

    function sendNotification($order,$status,$additional){
        $shopName = \Session::get('shop_name');

		$customer  = Models\Customer::where('email',$order->email)->first();
	
        if($additional){
            $message = $status->department_email_message;
			$email_object['to']=$additional;
			$email_object['name']='';
			$subject = (isset($status->department_email_subject))?$status->department_email_subject:'';
        }
        else{
            $message = $status->email_message;
			$email_object['to']=$order->email;
			$email_object['name']=(isset($customer->name))?$customer->name:'';
			$subject = (isset($status->email_subject))?$status->email_subject:'';
        }
        
        foreach($this->placeholders as $key=>$value){
            if($value =='name' )
			{
                if(isset($order->name)){
				 $message =str_replace($key,$order->name,$message);
				 $subject =str_replace($key,$order->name,$subject);
				}
				else
				{
					$message =str_replace($key,'',$message);
					$subject =str_replace($key,'',$subject);
				}
				
			}
            if($value =='custom_status_id' )
			{
                if(isset($status->external_name)){
				$message =str_replace($key,$status->external_name,$message);
				$subject =str_replace($key,$status->external_name,$subject);
				}
				else
				{
					$message =str_replace($key,'',$message);
					$subject =str_replace($key,'',$subject);
				}
				
			}
            if($value =='status' ){
			 if(isset($order->status)){
				$message =str_replace($key,$order->status,$message);
				$subject =str_replace($key,$order->status,$subject);
				}
				else
				{
					$message =str_replace($key,'',$message);
					$subject =str_replace($key,'',$subject);
				}
            }   
            if($value =='customer_name' )
			{
                if(isset($customer->name)){
				$message =str_replace($key,$customer->name,$message);
				$subject =str_replace($key,$customer->name,$subject);
				}
				else
				{
					$message =str_replace($key,'',$message);
					$subject =str_replace($key,'',$subject);
				}
			}
        }
        $message =str_replace('[shop-name]',$shopName,$message);
		$subject =str_replace('[shop-name]',$shopName,$subject);
        $data_object['view'] ='emails.statusChange';
        
		$email_object['subject']  = $subject;

        $data_object['data']=array('mes'=>$message);




        $email_from = Models\Settings::where('entity_name','email_from')->select('entity_value')->first();
        $from_name = Models\Settings::where('entity_name','email_from_name')->select('entity_value')->first();
        $email_object['from_email']=($email_from !='')?$email_from->entity_value:'no@reply.com';
        $email_object['from_name']=($from_name !='')?$from_name->entity_value:'';
        
		
		 $database =  $this->config['DB']['DB_DATABASE'];

                $connection =  array('default' => env('DB_CONNECTION', $database),
                    'connections'=>array($database => [
                        'driver'    => 'mysql',
                        'host'      => $this->config['DB']['DB_HOST'],
                        'database'  => $database,
                        'username'  => $this->config['DB']['DB_USERNAME'],
                        'password'  => $this->config['DB']['DB_PASSWORD'],
                        'charset'   => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                        'prefix'    => '',
                        'strict'    => false,
                    ]));
                Config::set('database', $connection);

        $email = new Lib\Email();
        $result = $email->queueEmail($data_object,$email_object);
    }


    /*  Code for background order loading */
    public function customOrders(){

        $input = \Input::all();
        $limit = 20;
        $offset = 0;
        $date_comapare=array('lte'=>'<=','gte'=>'>=','eq'=>'=');
        $sort = 'order_id';
        $sort_value = 'desc';
        $search ='';
        if(isset($input['sort'])){
            $sort = $input['sort'];
        }
        if(isset($input['order'])){
            $sort_value = $input['order'];
        }

        if(isset($input['offset']))
        {
            $offset = $input['offset'];
        }

        if(isset($input['limit']))
        {
            $limit = $input['limit'];
        }
        if(isset($input['search']))
        {
            $search = str_replace('#','',$input['search']);
        }

        $query = Models\Orders::query();
        $ordersData=array();

        if(isset($input['filter'])) {
            $filters = json_decode($input['filter']);

            foreach ($filters as $key => $value) {
                $arr=array();
                $name=$key;
                if($name=='order_created_at'){
                    foreach ($value as $key=>$vals) {
                        $query->whereDate($name, $date_comapare[$key],$vals );
                    }
                }
                else {
                    foreach ($value->_values as $vals) {
                        $arr[] = $vals;
                    }
                    $query->whereIn($name, $arr);
                }
            }

        }
        if($search){

            $query->whereIn('order_id', function($query) use ($search) {
                $query->select('order_id')->from(with(new Models\Orders)->getTable());
                $query->orWhere('financial_status', 'like', "%{$search}%");
                $query->orWhere('name', 'like', "%{$search}%");
                $query->orWhere('fulfillment_status', 'like', "%{$search}%");
                //$query->orWhere('total_price', 'like', "%{$search}%");
                $query->orWhere('order_created_at', 'like', "%{$search}%");
                $query->orWhere('status', 'like', "%{$search}%");
                $query->orWhereIn('custom_status_id', function($query) use ($search) {
                    $query->select('id')
                        ->from(with(new Models\CustomStatus)->getTable())
                        ->orWhere('external_name', 'like', "%{$search}%");

                });
                $query->orWhereIn('email', function($query) use ($search) {
                    $query->select('email')
                        ->from(with(new Models\Customer)->getTable())
                        ->orWhere('name', 'like', "%{$search}%");

                });


            });
        }
        $ordersData['total']=$query->count();
        $orders = $query->take($limit)->skip($offset)->orderBy($sort,$sort_value)->get();

        $ordersData['rows']=array();
        $i=$offset+1;
        if($orders)
        {
            foreach($orders as $order)
            {
                $customer  = Models\Customer::where('id',$order->customer_id)->first();
                $status =  Models\CustomStatus::find($order->custom_status_id);

                $orderData['order_id'] = $order->order_id;
                $orderData['s_no'] = $i++;
                $orderData['email'] = $order->email;
                $orderData['name'] = $order->name;
                $orderData['status'] = $order->status;
                $orderData['total_price'] = $order->total_price .' '.$order->currency;
                $orderData['order_created_at'] =  $orderData['order_created_at'] = date('Y-m-d',strtotime($order->order_created_at));
                $orderData['fulfillment_status'] = ($order->fulfillment_status!='')?$order->fulfillment_status:'Unfulfilled';
                $orderData['financial_status'] = $order->financial_status;
                $orderData['custom_status_id'] = $order->custom_status_id;
                $orderData['color'] =($status!='')?$status->color:'';
                $orderData['custom_status_name'] =($status!='')?$status->external_name:'Not Set';
                $orderData['customer_name'] = ($customer=='')?'':$customer->name;


                if ($this->get_brightness(  $orderData['color'] ) > 130) // will have to experiment with this number
                    $color = '000000';
                else
                    $color= 'ffffff';
                $orderData['text_color']= ($status!='')?$color:'000000';
                $ordersData['rows'][] = (object) $orderData;

            }
        }
		return response()->json($ordersData)->header('P3P', 'CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
        

    }
    public function emailCheck(){
        $data_object['view'] ='emails.statusChange';
        $data_object['data']=array('mes'=>'test email');
        $email_object['to']='er.renuka094@gmail.com';
        $email_object['name']='renuka';

        $email_object['from_email']='no@reply.com';
        $email_object['from_name']='';
        $email_object['subject'] = 'test mail';
        $email = new Lib\Email();
        $result = $email->queueEmail($data_object,$email_object);
		if( count(Mail::failures()) > 0 ) {

		   echo "There was one or more failures. They were: <br />";

		   foreach(Mail::failures as $email_address) {
			   echo " - $email_address <br />";
			}

		} else {
		echo "No errors, all sent successfully!";
		}
		
       // echo print_r($result). 'Mail send';
    }
	/* Code for After Accept/Decline Reccuring Application Charge */
    public function activate_app()
    {
       $charge_id = \Input::get('charge_id');
      
        $token=\Session::get('permanent_token');
        $shop=\Session::get('shop_name');
		if($token!='' && $shop!='') {
			$caller = new Shopify\ShopifyDialer();
			$caller->setShopName($shop);
			$caller->setAccessToken($token);

			$rac = new Shopify\RACHelper($caller);
			$rac->setId($charge_id);
			$res = $rac->getRACById();
		
			$res = json_decode($res->body);

			$res = $res->recurring_application_charge;
			$status = $res->status;
	    	if(isset($status) && $status != '') {
	    	
            $new_status =     $status;
            $status = ($new_status=='active')?'accepted':$new_status;
			$all_shop = $this->getShop($shop);
			$all_shop->payment_status = $status;
			$all_shop->save();

			$db  =\Session::get('database_name');
			$GLOBALS['database']=$db;
			$user_shop = $this->getShopByUser($shop);
			$user_shop->payment_status = $status;
			$user_shop->save();
            
            if ($status == 'accepted') {
                    if ($new_status == 'accepted') {

                        $test = $rac->activateRACById();
                        //Log::info(json_encode($test->headers));
                        if($test->headers['Status-Code']!=200){

                            $status = 'pending';
                        }

                        $all_shop = $this->getShop($shop);
                        $all_shop->payment_status = $status;
                        $all_shop->save();

                        $db = \Session::get('database_name');
                        $GLOBALS['database'] = $db;
                        $user_shop = $this->getShopByUser($shop);
                        $user_shop->payment_status = $status;
                        $user_shop->save();

                        if($test->headers['Status-Code']==200 && $all_shop->is_update==0 ){

                            $this->sync_app_data();
                            return redirect()->to('shopify/home');
                        }



                    }


                }
            
			
		  }	

		  return redirect()->to('https://'.$shop.'.myshopify.com/admin');

		  
		}
        return redirect()->to('https://apps.shopify.com');
    }
		/* Sync Data After User Accept Recurring Application Charge */
        function sync_app_data()
        {
            $permanentAccessToken=\Session::get('permanent_token');
            $shopName=\Session::get('shop_name');
            $caller = new Shopify\ShopifyDialer();
            $caller->setShopName($shopName);
            $caller->setAccessToken($permanentAccessToken);

            $shopHelper  = new Shopify\ShopHelper($caller);
            $data = $shopHelper->getShop();
            $data =$data->body;
            $shop = json_decode($data);
            Models\CustomStatus::seedData();  /* Seed default custom status */
            ShopSettingsController::customSettings($shop->shop);
            $email_to = Models\Settings::where('entity_name','shop_email_address')->select('entity_value')->first();
            $to_name = Models\Settings::where('entity_name','email_from_name')->select('entity_value')->first();
            if($email_to){
                $database =  $this->config['DB']['DB_DATABASE'];

                $connection =  array('default' => env('DB_CONNECTION', $database),
                    'connections'=>array($database => [
                        'driver'    => 'mysql',
                        'host'      => $this->config['DB']['DB_HOST'],
                        'database'  => $database,
                        'username'  => $this->config['DB']['DB_USERNAME'],
                        'password'  => $this->config['DB']['DB_PASSWORD'],
                        'charset'   => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                        'prefix'    => '',
                        'strict'    => false,
                    ]));
                Config::set('database', $connection);


                $data_object['view'] ='emails.welcome';
                $data_object['data']=array('mes'=>'','name'=>$to_name->entity_value);
                $email_object['to']=$email_to->entity_value;
                $email_object['name']=$to_name->entity_value;
                $email_object['from_email']= $this->config['MAIL']['EMAIL_FROM'];
                $email_object['from_name']= $this->config['MAIL']['EMAIL_FROM_NAME'];
                $email_object['subject'] = 'Welcome to Custom Order Status App';
                $email = new Lib\Email();
                $result = $email->queueEmail($data_object,$email_object);


                $data_object['view'] ='emails.order_sync';
                $data_object['data']=array('mes'=>'','name'=>$to_name->entity_value,'shop_name'=>$shopName);
                $email_object['to']=$email_to->entity_value;
                $email_object['name']=$to_name->entity_value;
                $email_object['from_email']= $this->config['MAIL']['EMAIL_FROM'];
                $email_object['from_name']= $this->config['MAIL']['EMAIL_FROM_NAME'];
                $email_object['subject'] = 'Order Sync under Progress - Custom Order Status App';

                $result = $email->queueEmail($data_object,$email_object);




            }

            $hooks = new Lib\WebHooks($shopName,$permanentAccessToken);
			$hooks->setAddress(\URL::to('/hooks'));
          // $hooks->setAddress('https://aca6781d.ngrok.io/laravel/public/hooks');
            $hooks->createWebHook('orders/create');
            $hooks->createWebHook('orders/updated');
            $hooks->createWebHook('orders/delete');
            $hooks->createWebHook('customers/create');
            $hooks->createWebHook('customers/update');
            $hooks->executeWebHook();



            $this->getOrders();
        }
		/* Show Order Status Details Page */
		public function orderDetails()
		{
            $inputs = \Input::all();
            if(isset($inputs['shop_name']))
            {
                $shop =$inputs['shop_name'];
            }
            else{
                $shop =$_SERVER['HTTP_REFERER'];
                $shop = explode('shop_name=',$shop);
                $shop =(isset($shop[1]))?$shop[1]:$shop[0];
            }

            $str = preg_replace('#^https?://#', '', $shop);
            $shop_name = $str;
            $shop = $this->getShop($str);
            if(isset($shop)) {
                $GLOBALS['database'] = $shop['database_name'];
                \Session::set('permanent_token',$shop['permanent_token']);
                \Session::set('shop_name',$shop['shop_name']);
				\Session::set('db_name',$shop['database_name']);
            }
            else{
                $GLOBALS['database'] = \Session::get('db_name');
            }
            $button = Models\Settings::where('entity_name','embed_button_label')->select('entity_value')->first();
            $button = (isset($button->entity_value)&& $button->entity_value!='')?$button->entity_value:'Submit';
            $layout = 'layouts.embed';
			return view($layout, ['content' => view('shopify/showOrder',array('button_label'=>$button,'shop_name'=>$shop_name))])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);

		}
		/* Show Order Status */
		public function doOrderDetails()
		{
			$input = \Input::all();
			$shop = $this->getShop($input['shop']);
            $shop_name = preg_replace('#^https?://#', '', $input['shop']);
			$input['order_id'] = trim(str_replace('#','',$input['order_id']));
			$token=$shop->permanent_token;
			$shop=$shop->shop_name;

			$caller = new Shopify\ShopifyDialer();
			$caller->setAccessToken($token);
			$caller->setShopName($shop);

			$orderHelper = new Shopify\OrderHelper($caller);
			$data = array('name' => $input['order_id'],'status'=>'any');
			$response = $orderHelper->getOrders($data);

			$response = json_decode($response->body);
			$orders = (isset($response->orders)) ? $response->orders : '';
			$checkemail = (isset($orders[0])) ? $orders[0] : '';

			if (isset($response->error) || !isset($orders[0]) || $checkemail->email != $input['email']) {
			\Session::flash('error_message', 'Invalid Order Id / Email Address');
			return \Redirect::to('shopify/order-details');
			}
			$button = Models\Settings::where('entity_name','embed_button_label')->select('entity_value')->first();
			$button = (isset($button->entity_value)&& $button->entity_value!='')?$button->entity_value:'Submit';

			$custom_status = Models\Orders::where('order_id','=',$checkemail->id)->leftjoin('shop_order_status','shop_order_status.id','=','shop_orders.custom_status_id')->first();

			$status='Unfulfilled';
			if(isset($checkemail->fulfillment_status))
			{
			$status = ($checkemail->fulfillment_status!='')?$checkemail->fulfillment_status:'Unfulfilled';
			}

			if(isset($custom_status->external_name))
			{
			$status=$custom_status->external_name;
			}

			$layout = 'layouts.embed';
			return view($layout, ['content' => view('shopify/showOrder', array('orders' => $orders,'button_label'=>$button,'status'=>$status,'shop_name'=>$shop_name))])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);
		}
		
		public function Intercom()
        {
            $shop_name = \Session::get('shop_name');
            if($shop_name) {
                $shop = $this->getShop($shop_name);
                if ($shop->database_name)
                {
                    $GLOBALS['database'] = $shop->database_name;
                    $settings = Models\Settings::where('entity_name', 'shop_email_address')->select('entity_value')->first();

                    if (isset($settings->entity_value)) {
                        $settings = array('shop_name' => $shop->shop_name, 'email' => $settings->entity_value);
                    }
                }
            }
            if(!$settings){
                $settings = array('shop_name' => 'unknown', 'email' => 'unknownuser@mail.com');
            }
            return $settings;
        }
		
		public function embedForm()
        {
            $shop = Models\Shop::first();
            $shop = (object)$shop;
            return view($this->layout, ['content' => view('shopify/embed',array('shop_name'=>$shop->shop_name))])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);;

        }

        public function getShopData()
        {
            return view($this->layout, ['content' => view('shopify/safari')])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);
        }

        public function FetchShopName()
        {
            $inputs = \Input::all();

            $str = preg_replace('#^https?://#', '', $inputs['shop_name']);
            $url=\URL::to('shopify/index').'?shop='.$str;
            return redirect()->to($url);
        }

        public function HelpAndSupport()
        {
            return view('layouts.help', ['content' => view('shopify/support')])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);
        }

        public function MACUserLogin()
        {
            return view($this->layout, ['content' => view('shopify/LoginLink')])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);
        }

        public function getOrderDetails()
        {
            $inputs = \Input::all();
            $order = Models\Orders::where('order_id','=',$inputs['order_id'])->leftjoin('shop_order_status','shop_order_status.id','=','shop_orders.custom_status_id')->first();

            return view('shopify/orderDetails',array('orders'=>$order));

        }

        public  function getBulkActions()
        {
            $inputs = \Input::all();
            foreach($inputs['data'] as $dta)
            {
                if($dta['name'] ==$inputs['actions'])
                {
                    Models\Orders::whereIn('order_id',$inputs['ids'])->update([$inputs['actions']=>$dta['value']]);

                    if($inputs['actions']=='custom_status_id')
                    {
                        $data = Models\Orders::whereIn('order_id',$inputs['ids'])->get();
                        $status = Models\CustomStatus::find($dta['value']);
                        if($data)
                        {
                            foreach($data as $order)
                            {
                                if($order->email !='' && $status->is_email ==1){
                                    $this->sendNotification($order,$status,false);
                                }

                                if($status->department_email !='' && $status->is_email_department ==1){
                                    $all_email = array_map('trim',explode(',',$status->department_email));
                                    $valid_email = array();
                                    foreach ( $all_email as $key => $email )
                                    {
                                        if(filter_var($email, FILTER_VALIDATE_EMAIL) != false){
                                            // email is invalid, do what you want
                                            $valid_email[] =$email;
                                        }
                                        // now email is valid
                                        // code for sending email
                                    }
                                    if($valid_email){
                                        $this->sendNotification($order,$status,$valid_email);
                                    }
                                }

                            }
                        }

                    }

                }
            }
            return '';
        }

    }
