<?php
 
namespace App\Http\Controllers\Shopify;
use App\Http\Controllers\Controller;
use App\Models;

class ShopSettingsController extends Controller
{
	protected $all_settings = array('email_from_name','email_from');
	protected $layout = 'layouts.defaults';
    public function __construct()
    {
        $this->middleware('select_db');
        $this->middleware('CheckBrowser',['except'=>['getShopData','orderDetails','doOrderDetails','MACUserLogin']]);
		$this->config =  \MainConfig::$config;
        $this->get_user_data();
    }
	//Show Email Settings
    public function Settings()
    {
        
		foreach($this->all_settings as $setting){
			$row = 	Models\Settings::where('entity_name',$setting)->first();
			if(!$row){
			 $row = new Models\Settings();
			
			 $row->entity_name = $setting;
			
			 $row->save();
			}
		}
		$settings = Models\Settings::all(); 
		return view($this->layout, ['content' => view('shopify.settings',array('settings'=>$settings))])->with('app_key', $this->config['Shopify']['SHOP_CLIENTID']);
        
    }
	/* Save Settings */
    public function doSettings()
    {
        $input=\Input::all();
		$tabs = isset($input['tabs'])?$input['tabs']:'';
		unset($input['tabs']);
		unset($input['_token']);
		foreach($input as $key=>$value ){
		$row = 	Models\Settings::where('entity_name',$key)->first();
			if(!$row){
				$row = new Models\Settings();
			}
		 $row->entity_name = $key;
		 $row->entity_value= $value;
		 $row->save();		 
		}
		
		\Session::flash('tabs',$tabs);
        \Session::flash('message'.$tabs,'Updated Successfully');
		 return $this->Settings();
      //  return View('shopify.settings',array('settings'=>$settings));
    }
	/* Save Custom Settings */
	public static function customSettings($data)
    {
        $arr = array('email_from_name'=>$data->name,'email_from'=>$data->email,'shop_email_address'=>$data->email);
        foreach($arr as $key=>$value ) {
            $row = Models\Settings::where('entity_name', $key)->first();
            if (!$row) {
                $row = new Models\Settings();
            }
            $row->entity_name = $key;
            $row->entity_value = $value;
            $row->save();
        }
    }

}
