<?php

namespace App\Http\Controllers;
set_time_limit(0);
header('p3p: CP="NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM"'); header('Access-Control-Allow-Credentials: true');
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    public function __construct()
    {
		$this->middleware('select_db',['except'=>['index','oauth']]);

        // Here's something that happens after the request
        $this->afterFilters(function() {
            Session::save();
        });
    }

    public function get_user_data()
    {
        if(\Session::get('shop_name')!='')
        {
            $GLOBALS['database'] = $this->config['DB']['DB_DATABASE'];

            $shop = Models\ShopUsers::getShopUserByShopName(\Session::get('shop_name'));
            $GLOBALS['shop_data'] = json_encode($shop);
        }
    }
}
