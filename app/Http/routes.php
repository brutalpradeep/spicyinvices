<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect()->to('https://apps.shopify.com/custom-order-status');
});
Route::get('shopify/index','Shopify\ServeController@index');
Route::get('shopify/home','Shopify\ServeController@home');
Route::get('shopify/oauth','Shopify\ServeController@oauth');


Route::post('shopify/uninstall','Shopify\ServeController@uninstall');

Route::get('shopify/checkMail','Shopify\ServeController@uninstall');

Route::get('shopify/auth','Shopify\ServeController@shopifyAuth');
Route::get('shopify/callback','Shopify\ServeController@shopifyCallback');
Route::get('shopify/shop','Shopify\ServeController@Shop');
Route::get('shopify/order','Shopify\ServeController@getOrder');
Route::get('shopify/product','Shopify\ServeController@getProduct');


Route::get('shopify/queue','Shopify\ServeController@getQueue');

Route::post('hooks/{type}/{event}','Shopify\WebHookController@get_WebHooks_Request');
Route::get('hooks/webhooks','Shopify\WebHookController@allHooks');


Route::get('shopify/orders/status','Shopify\CustomStatusController@CustomOrderStatus');
Route::get('shopify/orders/status/edit','Shopify\CustomStatusController@CustomOrderStatusEdit');
Route::post('shopify/orders/status/create','Shopify\CustomStatusController@getCustomOrderStatus');
Route::post('shopify/orders/status/delete','Shopify\CustomStatusController@CustomOrderStatusDelete');
Route::post('shopify/orders/status/disable','Shopify\CustomStatusController@CustomOrderStatusDisable');


Route::get('shopify/settings','Shopify\ShopSettingsController@Settings');
Route::post('shopify/settings','Shopify\ShopSettingsController@doSettings');

Route::post('shopify/process/{action?}/{value?}/{order_id?}','Shopify\ServeController@updateOrder');
Route::get('shopify/orders','Shopify\ServeController@showOrders');
Route::post('shopify/process','Shopify\ServeController@updateOrder');
Route::get('shopify/orders','Shopify\ServeController@showOrders');
Route::get('shopify/orders/get','Shopify\ServeController@customOrders');
Route::get('shopify/mail','Shopify\ServeController@emailCheck');

Route::get('shopify/activate','Shopify\ServeController@activate_app');

Route::get('shopify/order-details','Shopify\ServeController@orderDetails');
Route::post('shopify/order-details','Shopify\ServeController@doOrderDetails');
Route::get('shopify/embed-form','Shopify\ServeController@embedForm');

Route::post('shopify/intercom','Shopify\ServeController@Intercom');

Route::get('shopify/user-login','Shopify\ServeController@MACUserLogin');
Route::get('shopify/getShopName','Shopify\ServeController@getShopData');
Route::post('shopify/getShopName','Shopify\ServeController@FetchShopName');

Route::get('shopify/help','Shopify\ServeController@HelpAndSupport');
Route::post('shopify/order/details','Shopify\ServeController@getOrderDetails');

Route::post('shopify/bulk/actions','Shopify\ServeController@getBulkActions');