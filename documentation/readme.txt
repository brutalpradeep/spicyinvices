1. install composer in project directory using command (php -r "readfile('https://getcomposer.org/installer');" | php)

2. Composer Update using command (php composer.phar update) 

3. Copy app.example.yml  and rename it app.yml file.

4. Create Database and set configuration in (app.yml file).


5. Add shopify app Cliend id and secret in (app.yml file).

6. Add plan details in in (app.yml file).

7. Publish config file using command

  php artisan vendor:publish

commands
1. php artisan migrate
2. php artisan MyMigration
3. php artisan queue:work --database

4. sudo crontab -e
 add cron ====== * * * * * /path/to/artisan schedule:run >> /dev/null 2>&1  
e.g. * * * * * /path/to/artisan(/usr/bin/php5 /home/ubuntu/workspace/artisan) schedule:run >> /dev/null 2>&1  

Install supervisor
1. sudo apt-get install supervisor
2. sudo vim /etc/supervisor/conf.d/laravel-worker.conf
3. add these lines in laravel-worker.conf file
	[program:cos]
	process_name=%(program_name)s_%(process_num)02d
	command=php /home/ubuntu/workspace/artisan queue:work database --queue=default
	autostart=true
	autorestart=true
	numprocs=2
	user=www-data
	redirect_stderr=true
	stdout_logfile=/var/www/html/cos/storage/logs/worker.log

4. sudo vim /etc/supervisor/conf.d/laravel-queue.conf
5. add these lines in laravel-queue.conf file
	[program:cos_queue]
	process_name=%(program_name)s_%(process_num)02d
	command=php /home/ubuntu/workspace/artisan queue:work database --queue=high
	autostart=true
	autorestart=true
	numprocs=2
	user=www-data
	redirect_stderr=true
	stdout_logfile=/var/www/html/cos/storage/logs/worker_queue.log

4. sudo supervisorctl
->rearead
->add laravel-worker
->add laravel-queue

Check the database and job table status
1. php artisan DBStatus




