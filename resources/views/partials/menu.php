<div class="menu-section">
<div class="container">
<nav class="navbar navbar-default">
    <div class="row">
        <div class="pull-right">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo URL::to('/shopify/home');?>">Home</a></li>
                <li class=""><a href="<?php echo URL::to('/shopify/orders');?>">Orders</a></li>
                <li><a href="<?php echo URL::to('/shopify/orders/status')?>">Custom Status</a></li>
                <li><a href="<?php echo URL::to('/shopify/settings')?>">Settings</a></li>
                <li><a href="<?php echo URL::to('/shopify/embed-form')?>">Order Lookup</a></li>
                <li><a href="<?php echo URL::to('/shopify/help')?>">Help & Support</a></li>
            </ul>
        </div>
    </div>
</nav>
</div>
</div>

<div>
    <?php
    $being_synced=0;
    if(isset($GLOBALS['shop_data']))
    {
        $shop =(object) json_decode($GLOBALS['shop_data']);
        $being_synced=$shop->being_synced;
    }

    ?>
    <?php if($being_synced==1) {
        ?>
        <div class="alert alert alert-warning fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            We are currently syncing Your data. Syncing can take up sometime depending on size of Your Shop's data.
        </div>
    <?php } ?>
</div>