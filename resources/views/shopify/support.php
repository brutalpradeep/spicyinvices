
<!----Content Section------------------>
<div class="container content">
    <div class="row">
        <div class="col-md-12">
            <h2>For Support contact us at :<a href="#">support@spicegems.com</a></h2>
            <div class="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseOne"> <span class="fa fa-plus"></span>
                                Create Custom Status
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body help-content">
                            <p>The order list screen displays all open orders from your store. You can assign custom status to any of the order. Simply click on “Not Set” and select the status to be assigned. Assigning a status will also trigger any corresponding email notification (s)</p>
                            <img src="<?php echo asset('images/custom_status.gif'); ?>" class="img-responsive">

                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseTwo"><span class="fa fa-plus"></span>
                                Assign Status to Order
                            </a>
                        </h4>
                    </div>


                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body help-content">
                            <p>Go to Custom Status Menu and Click on Add Status Button. Now you will be presented with the status creation screen. You can set status name, color code and associated notification to be sent on status change.</p>
                            <img src="<?php echo asset('images/order.gif'); ?>" class="img-responsive">
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseThree"><span class="fa fa-plus"></span>
                                Create a Personalized Notification Email
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body help-content">
                            <p>You can add / edit notification email from the status creation screen</p>
                            <img src="<?php echo asset('images/email.gif'); ?>" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent=".accordion" href="#collapseFourth"><span class="fa fa-plus"></span>
                                Insert Order Lookup Form
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFourth" class="panel-collapse collapse">
                        <div class="panel-body help-content">
                            <p>Go to the Order Lookup Menu and Copy the provided code. Simply paste the code on any Shopify page where you want the Lookup form to appear.</p>
                            <img src="<?php echo asset('images/order_lookup.gif'); ?>" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
