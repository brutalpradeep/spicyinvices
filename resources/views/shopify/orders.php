<div class="container box">
    <div class="panel-body orders_all">
    <div id="log"></div>
    Add Filter<div id="filter-bar"></div>
        <div class="bars pull-left">
            <div class="btn-group filter-group bulk_actions">
                <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown">
                    Bulk Actions <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#" data-value="custom_status_id">Change Custom Status</a></li>
                    <li><a href="#" data-value="admin_note">Add Note</a></li>
                </ul>
            </div>
        </div>

<table id="order" class="searchable table table-bordered table-heading"
       data-toolbar="#toolbar"
       data-search="true"
       data-show-refresh="true"
       data-show-toggle="false"
       data-show-columns="false"
       data-show-export="false"
       data-detail-view="false"
       data-detail-formatter="detailFormatter"
       data-minimum-count-columns="2"
       data-show-pagination-switch="false"
       data-pagination="true"
       data-id-field="order_id"
       data-side-pagination="server"
	   data-sort-order="desc"
       data-url="<?php echo URL::to('/shopify/orders/get'); ?>"
	   data-page-number="1"
       data-page-size="20"
       data-page-list="[10, 20, 50, 100, ALL]"
       data-show-footer="false"
       data-filter-control="true">

                        <thead>
                        <tr>
                            <th data-field="s_no">S. No.</th>
                            <th data-field="name" data-sortable="true">Order</th>
                            <th data-field="order_created_at" data-sortable="true">Date</th>
                            <th data-field="customer_name" data-sortable="true">Customer Name</th>
                            <th data-field="custom_status_id" data-filterable="true" data-sortable="true"  >Custom Status</th>
                            <th data-field="status" data-sortable="true" data-filterable="true">Status</th>
                            <th data-field="fulfillment_status" data-sortable="true" >Fulfillment Status</th>
                            <th data-field="financial_status" data-sortable="true" >Payment Status</th>


                        </tr>
                        </thead>
                    </table>
    </div>
    <div id="shop_name" style="display: none;"><?php echo \Session::get('shop_name');?></div>

    <div id="order_details" style="display:none"></div>

                </div>


<script>
var source = JSON.parse('<?php echo $all_status; ?>');

var custom_status = '<?php echo $filters['custom_status']; ?>';
custom_status = JSON.parse(custom_status);

var payment_status = '<?php echo $filters['payment_status']; ?>';
payment_status = JSON.parse(payment_status);

var fulfillment_status = '<?php echo $filters['fulfillment_status']; ?>';
fulfillment_status = JSON.parse(fulfillment_status);

var order_status = '<?php echo $filters['order_status']; ?>';
order_status = JSON.parse(order_status);

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="status_all">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Custom status of selected orders</h4>
            </div>
            <div class="modal-body">
                <form name="all_settings">
                    <input type="hidden" id="action">
                    <div class="status_div all_div">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Select Custom Status</label>
                            <select class="form-control com-md-4" id="change_all_status" name="custom_status_id" style="width:200px"></select>
                        </div>
                    </div>

                    <div class="note_div all_div">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Add Note</label>
                            <textarea name="admin_note" class="form-control" rows="10" cols="10"></textarea>
                        </div>
                    </div>
                    <div class="alert alert-info info_box" role="alert">
                        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary status_all_btn">Apply</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->