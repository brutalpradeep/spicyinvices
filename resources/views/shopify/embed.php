

    <div class="col-md-6 col-sm-offset-3 box">
       <div class="panel-body">
                <h3>Embed Orders Details Form</h3>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Copy the following code and Paste it in any WebPage</label>
                        <textarea class="form-control" rows="10" cols="50" onclick="$(this).select()" spellcheck="false">
<div name="OrderLookupDiv" id="OrderLookupDiv" ></div><script  type="text/javascript" src="<?php echo URL::to('assets/embedOrderLookup.js');?>"></script><script type="text/javascript">EmbedManager.embed({url: "<?php echo URL::to('/shopify/order-details'); ?>?shop_name=<?php echo $shop_name; ?>",width: "100%"});iFrameResize({checkOrigin:false});</script></textarea>
                    </div>
					<div class="highlight">
               <pre class="bg-info"><h4>Embed <b>Order LookUp Form</b></h4><h5>
1) Create a New Page.<br>
2) Copy the Above code in the Page and save the page</br>
3) Customers can now access the Order Lookup Form by accessing the Page.
               </h5></pre>
           </div>
        </div>
    </div>