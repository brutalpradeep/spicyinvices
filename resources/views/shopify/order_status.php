
<div class="container box">
    <div clas="col-md-9">
    <div class="row">
            <div class="col-md-12 all_div" id="all_orders_status_div">
                <div class="content">
                   <h3>Order Status</h3>
                </div>
                <div class="form-group pull-right">
                    <button id="add_custom_status" class="btn btn-success">Add New Status</button>
                </div>

                <div class="bars pull-left">
                    <div id="toolbar">
                        <button id="button" class="btn btn-danger">Delete</button>
                    </div>
                </div>
<table id="table" class="table table-bordered table-heading"
       data-toolbar="#toolbar"
       data-search="true"
       data-show-refresh="true"
       data-show-toggle="false"
       data-show-columns="false"
       data-show-export="false"
       data-detail-view="false"
       data-detail-formatter="detailFormatter"
       data-minimum-count-columns="2"
       data-show-pagination-switch="false"
       data-pagination="true"
       data-id-field="order_id"
       data-page-number="1"
       data-page-size="10"
       data-page-list="[10, 20, 50, 100, ALL]"
       data-show-footer="false"

       data-side-pagination="client"
       
       data-response-handler="responseHandler">

               
                   <thead>
                    <tr>
                        <th data-field="state" data-checkbox="true" ></th>
                        <th data-field="id" data-sortable="true">Id</th>
                        <th data-field="internal_name" data-sortable="true">Internal Name</th>
                        <th data-field="external_name" data-sortable="true">External Name</th>
                        <th data-field="color" data-sortable="true">Color</th>
                        <th data-field="email_notifications" data-sortable="true">Email Notifications</th>
                        <th data-field="status" data-sortable="true">Status</th>
                        <th data-field="actions">Actions</th>
                    </tr>
                   </thead>
                    <tbody>

                    <?php
                    $i=0;
                    foreach($order_status as $value){ ?>
                        <tr id="row<?php echo $value->id; ?>" class="<?php if($value->is_active==0){echo 'disable';} ?>">

                            <td class="bs-checkbox"><input data-index="<?php echo $i; ?>" name="btSelectItem" type="checkbox"></td>

                            <td><?php echo $i+1; ?></td>

                            <td><?php echo $value->internal_name; ?></td>
                            <td><?php echo $value->external_name; ?></td>

                            <td><a style="background-color: #<?php echo $value->color; ?>;" href="javascript:void(0)">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a></td>

                            <td><?php if($value->is_email==1){echo 'Yes';}else{echo 'No';} ?></td>

                            <td><div><a href="javascript:void(0)" data-pk="<?php echo $value->id; ?>" data-title="Select status" data-name="custom_status" data-value="<?php echo $value->is_active; ?>" class="disable_order_status"> <?php if($value->is_active==1){echo 'Enabled';}else{echo 'Disabled';} ?></a></div></td>

                            <td><div style="float: left"><a href="javascript:void(0)" class="edit_status" data-id="<?php echo $value->id; ?>">Edit</a></div>
                            </td>

                        </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>

        <div id="custom_status_div" class="col-md-8  col-sm-offset-2  all_div" style="display: none;">

            <h2>Order Status</h2>
            <form id="frm_order_status">

                <div class="form-group">
                    <label for="exampleInputEmail1">Internal Name</label>
                    <input type="text" class="form-control" name="internal_name" id="internal_name" placeholder="Internal Name">
                    <input type="hidden" name="order_status_id" >
                    <input type="hidden" id="order_status_action" name="order_status_action" >
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">External Name</label>
                    <input type="text" class="form-control" name="external_name" id="external_name" placeholder="External Name">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Color</label>
                    <input type="text"  name="color" id="color" value="#000000">
                </div>
                <div class="col-md-12"><div class="divider col-md-4  col-sm-offset-4"></div></div>
                <div class="checkbox">
                    <label>

                        <input type="checkbox" name="is_email" id="is_email" checked> Send a Notification to Customer on Status Change

                    </label>
                </div>

                <div class="form-group email_notification">
                    <label for="exampleInputEmail1">Email Subject</label>
                    <input type="text" class="form-control" name="email_subject" id="email_subject" placeholder="Subject">
                </div>

                <div class="form-group email_notification">
                    <label for="exampleInputEmail1">Email Message</label>
					<div class="highlight">
                        <pre class="bg-info"><h5>Use these keys in <b>Email Subject</b> and <b>Email Message</b></br>[order-number] [custom-status] [shopify-order-status] [customer-name] [shop-name]</h5></pre>
                    </div>
                    <textarea class="form-control ckeditor" name="email_message" id="email_message"></textarea>
                </div>

                <div class="col-md-12"><div class="divider col-md-4  col-sm-offset-4"></div></div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="is_email_department" id="is_email_department"> Send additional Notification on Status Change
                    </label>
                </div>
                <div class="form-group email_department">
                    <label for="exampleInputEmail1">To Email Address </label>
                    <input type="text" class="form-control" name="department_email" id="department_email" placeholder="Comma Separate Multiple Email Address" required>
                </div>

                <div class="form-group email_department">
                    <label for="exampleInputEmail1">Subject </label>
                    <input type="text" class="form-control" name="department_email_subject" id="department_email_subject" placeholder="Email subject here">
                </div>

                <div class="form-group email_department">
                    <label for="exampleInputEmail1">Message</label>
					
					<div class="highlight">
                        <pre class="bg-info"><h5>Use these keys in <b>Email Subject</b> and <b>Email Message</b></br>[order-number] [custom-status] [shopify-order-status] [customer-name] [shop-name]</h5></pre>
                    </div>
                    <textarea class="form-control ckeditor" name="department_email_message" id="department_email_message"></textarea>
                </div>

                <button type="submit" id="save_order_status" class="btn btn-default">Submit</button>
                <button type="button" id="hide_add_order_status" class="btn btn-warning">Cancel</button>
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<input type="hidden" id="index" value="">
            </form>
            <br>
        </div>


        </div>
  </div>
</div>
<script>
var page_path='<?php echo URL::to('/'); ?>';
var status_rows='<?php echo $status_count; ?>';
var count_status='<?php echo count($order_status); ?>';
window.CKEDITOR_BASEPATH = page_path+'/assets/bower/ckeditor/';
</script>
</div>
