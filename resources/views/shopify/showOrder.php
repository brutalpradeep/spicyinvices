<?php use App\Http\Controllers\Shopify; ?>
<?php  if(!isset($orders)){ ?>
            <div class="col-md-12 padding2">
                <div class="box-shadow">
               <div class="panel-body">
                        <h3>Orders</h3>
                        <?php if(Session::has('error_message')) { ?>
                            <div class="alert alert alert-success" role="alert">
                                <?php echo Session::get('error_message'); ?>
                            </div>
                        <?php } ?>
                        <form method="post" action="<?php echo URL::to('shopify/order-details'); ?>">
                            <div class="form-group">
                                <label for="exampleInputFile">Enter Order Number</label>
                                <input type="test" class="form-control" name="order_id" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" name="email" placeholder="example@yourmail.com" required>
                            </div>

                            <button type="submit" class="btn btn-default"><?php echo $button_label; ?></button>
                            <input type="hidden" name="shop" value="<?php echo $shop_name; ?>">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        </form>
                </div>
                </div>
            </div>
                <?php
                }
                else{
                    $orders = $orders[0];
                    $created_at = new DateTime($orders->created_at);
                    $created_at = $created_at->format('Y-m-d');

                    $updated_at = new DateTime($orders->updated_at);
                    $updated_at = $updated_at->format('Y-m-d');

                    ?>


                    <!----Content Section------------------>
                            <div class="order-info cus-lookup">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="order-info-head">
                                            <div class="panel-body">
                                                <h2>Order Number : <?php echo $orders->name ?></h2>
                                                <h4>Order Status : <?php echo $status ?></h4>
                                                <p>Order Date : <?php echo $created_at ?></p>
                                                <p>Order Updated Date : <?php echo $updated_at ?></p>
                                                <p>Order Email : <a href="mailto:<?php echo $orders->email ?>"><?php echo $orders->email ?></a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                <?php if(isset($orders->billing_address)) { ?>
                                    <div class="col-md-6 "  style="width:50%;float: left;">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4>Billing Information</h4>
                                            </div>
                                            <?php $billing_address = $orders->billing_address; ?>
                                            <div class="panel-body">
                                                <address>
                                                    <h4><?php echo ucfirst($billing_address->name); ?></h4>
                                                    <?php echo $billing_address->address1 . ' ' . $billing_address->address2; ?><br>
                                                    <?php echo $billing_address->city . ' ' . $billing_address->province ?><br>
                                                    <?php echo $billing_address->country . ' ' . $billing_address->zip; ?><br>
                                                    Phone: <?php echo $billing_address->phone ?>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                if(isset($orders->shipping_address)){ ?>
                                    <div class="col-md-6 text-right" style="width:50%;float: left;">
                                        <div class="panel">
                                            <?php $shipping_address = $orders->shipping_address; ?>
                                            <div class="panel-heading">
                                                <h4>Shipping Information</h4>
                                            </div>
                                            <div class="panel-body">
                                                <address>
                                                    <h4><?php echo ucfirst($shipping_address->name); ?></h4>
                                                    <?php echo $shipping_address->address1.' '.$shipping_address->address2 ?><br>
                                                    <?php echo $shipping_address->city.' '.$shipping_address->province; ?><br>
                                                    <?php echo $shipping_address->country.' '.$shipping_address->zip; ?><br>
                                                    Phone: <?php echo $shipping_address->phone ?>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                                <!-- / end client details section -->
                                <div class="lookup-order-table table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <h4>Products</h4>
                                                </th>
                                                <th class="text-center">
                                                    <h4>Quantity</h4>
                                                </th>
                                                <th class="text-center">
                                                    <h4>Price</h4>
                                                </th>
                                                <th class="text-center">
                                                    <h4>Total</h4>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $products = $orders->line_items;
                                            foreach ($products as $value) {
                                            ?>
                                                <tr>
                                                    <td><a href="javascript:void(0)"><?php echo $value->name; ?></a></td>
                                                    <td class="text-center"><?php echo $value->quantity; ?></td>
                                                    <td class="text-center"><?php echo $value->price; ?></td>
                                                    <td class="text-center"><?php echo ($value->price*($value->quantity));echo ' ('.$orders->currency.')'; ?> </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>



                                        </tbody>
                                    </table>

                                </div>

                                <div class="row order-total">
                                    <div class="col-xs-3 col-xs-offset-6 text-right">
                                        <p>
                                            Subtotal  :
                                        </p>
                                    </div>
                                    <div class="col-xs-3 text-center">
                                        <?php echo $orders->subtotal_price.' ('.$orders->currency.')'; ?>
                                    </div>
                                </div>


                                <?php
                                foreach($orders->tax_lines as $taxes)
                                {
                                ?>
                                <div class="row order-total">
                                    <div class="col-xs-3 col-xs-offset-6 text-right">
                                        <p>
                                            Tax  :
                                        </p>
                                    </div>
                                    <div class="col-xs-3 text-center">
                                        <?php echo $taxes->price.' ('.$orders->currency.')'; ?>
                                    </div>
                                </div>
                                <?php
                                }
                                foreach($orders->shipping_lines as $shipping)
                                {
                                ?>
                                <div class="row order-total">
                                    <div class="col-xs-3 col-xs-offset-6 text-right">
                                        <p>
                                            Shipping :
                                        </p>
                                    </div>
                                    <div class="col-xs-3 text-center">
                                        <?php echo $shipping->price.' ('.$orders->currency.')'; ?>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>

                                <div class="row order-total">
                                    <div class="col-xs-3 col-xs-offset-6 text-right">
                                        <p>
                                            Total :
                                        </p>
                                    </div>
                                    <div class="col-xs-3 text-center">
                                        <?php echo $orders->total_price; echo ' ('.$orders->currency.')'; ?>
                                    </div>
                                </div>

                                <div class="btn-area">
                                    <button type="button" class="btn btn-info" onclick="location.href='<?php echo URL::to('shopify/order-details').'?shop_name='.$shop_name; ?>'"><< Back</button>
                                </div>
                            </div>

                <?php
                }?>




