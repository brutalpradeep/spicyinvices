<?php

if(isset($orders->admin_note)){
    $admin_note = ($orders->admin_note!='')?$orders->admin_note:'';
}

?>

            <div class="col-md-8 col-md-offset-2">
                <div class="order-btn-top">
                    <button class="btn btn-primary back_to_orders"><i class="fa fa-arrow-left"></i>  Back to Orders</button>
                    <a class="btn btn-primary show_on_shopify" target="_blank"> View On Shopify <i class="fa fa-external-link"></i></a>
                </div>

                <div class="box-shadow">
                <div class="panel-body">
                    <h3>Orders Details</h3>
                    <div>
                        <?php
                            $status='Unfulfilled';
                            if(isset($orders->fulfillment_status)){
                                $status = ($orders->fulfillment_status!='')?$orders->fulfillment_status:'Unfulfilled';
                            }

                            if(isset($orders->external_name)){ $status=$orders->external_name; }

                            $created_at = new DateTime($orders->created_at);
                            $created_at = $created_at->format('Y-m-d');

                            $updated_at = new DateTime($orders->updated_at);
                            $updated_at = $updated_at->format('Y-m-d');
                        $data_order = json_decode($orders->data);

                        ?>
                        <p><h2>Order Number: <?php echo $orders->name ?></h2></p>
                        <p><h4>Order Status: <?php echo $status ?></h4></p>
                        <p><h5>Order Date: <?php echo $created_at ?></h5></p>
                        <p><h5>Order Updated Date: <?php echo $updated_at ?></h5></p>
                        <p><h5>Order Email: <?php echo $orders->email ?></h5></p>
                    </div>
                    <?php if(isset($data_order->billing_address)) { ?>
                        <div class="pull-left" style="width:50%;">
                            <h3>Billing Information</h3>
                            <?php $billing_address = $data_order->billing_address; ?>
                            <p><h5><?php echo $billing_address->name ?></h5></p>
                            <p>
                            <h5><?php echo $billing_address->address1 . ' ' . $billing_address->address2; ?></h5></p>
                            <p>
                            <h5><?php echo $billing_address->city . ' ' . $billing_address->province ?></h5></p>
                            <p>
                            <h5><?php echo $billing_address->country . ' ' . $billing_address->zip; ?></h5></p>
                            <p><h5>Phone: <?php echo $billing_address->phone ?></h5></p>
                        </div>
                    <?php
                    }
                    if(isset($data_order->shipping_address)){ ?>
                        <div class="pull-right" style="width:50%;">
                            <h3>Shipping Information</h3>
                            <?php $shipping_address = $data_order->shipping_address; ?>
                            <p><h5><?php echo $shipping_address->name ?></h5></p>
                            <p><h5><?php echo $shipping_address->address1.' '.$shipping_address->address2 ?></h5></p>
                            <p><h5><?php echo $shipping_address->city.' '.$shipping_address->province; ?></h5></p>
                            <p><h5><?php echo $shipping_address->country.' '.$shipping_address->zip; ?></h5></p>
                            <p><h5>Phone: <?php echo $shipping_address->phone ?></h5></p>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>

                    <table class="table table-bordered">
                        <thead>
                            <tr class="active">
                                <th>Products</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $products = $data_order->line_items;
                        foreach ($products as $value) {
                            ?>
                            <tr>
                                <td> <?php echo $value->name; ?> </td>
                                <td> <?php echo $value->quantity; ?> </td>
                                <td> <?php echo $value->price; ?> </td>
                                <td> <?php echo ($value->price*($value->quantity));echo ' ('.$orders->currency.')'; ?> </td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="3">Product Subtotal </td>
                                <td> <?php echo $data_order->subtotal_price.' ('.$orders->currency.')'; ?> </td>
                            </tr>
                            <?php
                            foreach($data_order->tax_lines as $taxes)
                            {
                                ?>
                                <tr>
                                    <td colspan="3"> <b>Tax</b> </td>
                                    <td> <?php echo $taxes->price.' ('.$orders->currency.')'; ?> </td>
                                </tr>
                            <?php
                            }

                            foreach($data_order->shipping_lines as $shipping)
                            {
                                ?>
                                <tr>
                                    <td colspan="3"><b> Shipping</b> </td>
                                    <td> <?php echo $shipping->price.' ('.$orders->currency.')'; ?> </td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr class="active">
                                <td><b>Total</b> </td>
                                <td></td>
                                <td></td>
                                <td><?php echo $orders->total_price; echo ' ('.$orders->currency.')'; ?></td>
                            </tr>
                        </tbody>
                    </table>

            </div>
        </div>
            <br />
            <div class="box-shadow">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Admin Notes</label>
                        <p>
                            <a href="javascript:void(0)" id="admin_notes" data-value="<?php echo $admin_note; ?>" data-type="textarea" data-pk="<?php echo $orders->order_id; ?>" data-url="process" data-name="admin_note"  class="editable editable-click" data-title="Enter Admin Note"><?php echo $admin_note; ?></a></p>
                    </div>
                </div>
            </div>
    </div>
