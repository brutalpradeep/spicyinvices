<div class="container content">
    <div class="row">
        <div class="col-md-5 col-sm-offset-4">
            <div class="instruction-box box">
                <div class="panel-heading">Please Enter your Store's url to login</div>
                <div class="panel-body">
                    <form  class="form-horizontal col-md-8">
                        <div id="shop_form_id" class="form-group">
                        <input type="hidden" value="<?php echo \URL::to('shopify/index') ?>" id="app_url">
                        <input type="text" placeholder="your-shop-url.myshopify.com" class="form-control" name="shop_name" id="shop_name" required>
                        <span id="error_msg" class="help-block"></span>
                        <button type="button" class="btn btn-primary" id="redirect_to_shop"> Login</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>