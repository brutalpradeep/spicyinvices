<div class="container content">
    <div class="row">
        <div class="col-md-5 col-sm-offset-4">
            <div class="instruction-box box">
                <div class="panel-heading">Create Custom Status</div>
                <div class="panel-body">
                    <p>1. Create Color Coded Custom Status Message.</p>
                    <p>2. Setup Triggered Email Notifications.</p>
                    <a href="<?php echo URL::to('/shopify/orders/status');?>" class="btn btn-primary pull-right">Create Status</a>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-offset-4">
            <div class="instruction-box box">
                <div class="panel-heading">View Order List</div>
                <div class="panel-body">
                    <p>1. View Complete List of Orders.</p>
                    <p>2. Filter the Orders Based on Shopify Status, Custom Status, Payment Status and dates</p>
                    <p>3. Toggle Custom Status with a Single Click</p>
                    <a href="<?php echo URL::to('/shopify/orders');?>" class="btn btn-primary pull-right">View orders</a>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-offset-4">
            <div class="instruction-box box">
                <div class="panel-heading">Configure Order LookUp Form</div>
                <div class="panel-body">
                    <p>Embed an Order LookUp Form on your Shopify Store. Customers can view the Order Status through the Lookup Form</p>
                    <a href="<?php echo URL::to('/shopify/embed-form');?>" class="btn btn-primary pull-right">Configure Order LookUp</a>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-offset-4">
            <div class="instruction-box box">
                <div class="panel-heading">Misc Settings</div>
                <div class="panel-body">
                    <p>1. Configure Email Settings</p>
                    <a href="<?php echo URL::to('/shopify/settings');?>" class="btn btn-primary pull-right">View Settings</a>
                </div>
            </div>
        </div>

    </div>
</div>