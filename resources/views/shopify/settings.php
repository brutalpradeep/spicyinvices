
<div class="col-md-8 col-sm-offset-2">
    <div class="row">
        <div class="panel-body">


            <!-----------Sidebar--------------->
            <div class="col-md-3">
                <div class="box padd-bottom">
                    <ul class="list-group nav-tab-wrapper" id="myTab">
                        <li class="<?php if(Session::get('tabs')==''){ echo 'active';} ?>">
                            <a href="#email_settings" data-toggle="tab"><i class="fa fa-credit-card"></i>Email Settings</a>
                        </li>
                        <li class="<?php if(Session::get('tabs')=='embedsettings'){ echo 'active';} ?>">
                            <a href="#embedsettings" data-toggle="tab"><i class="fa fa-cog"></i>Embed Form Settings</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!---------------/sidebar------->

            <!------Main Content------------>
            <div class="col-md-8 col-md-offset-1 feature-content box">
                <div class="tab-content" id="myTabContent">


                    <div id="email_settings" class="tab-pane fade <?php if(Session::get('tabs')==''){ echo 'in active';} ?>">
                        <div class="col-sm-12 feature-col">
                            <div id="settings"  class="tab-pane fade in active">
                                <h3>Email Settings</h3>
                                <?php if(Session::has('message')) {
                                    ?>
                                    <div class="alert alert alert-success" role="alert">
                                        <?php echo Session::get('message'); ?>
                                    </div>
                                <?php } ?>
                                <form class="col-md-9" id="" action="<?php URL::to('shopify/settings');?>" method="post">
                                    <?php foreach($settings as $setting){
                                        if($setting->entity_name == 'email_from_name'){
                                            ?>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">From Name</label>
                                                <input type="text" class="form-control" value="<?php echo $setting->entity_value; ?>" name="email_from_name" id="email_from_name" placeholder="From Name">
                                            </div>
                                        <?php }
                                        if($setting->entity_name == 'email_from'){							?>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">From Email</label>
                                                <input type="email" class="form-control" value="<?php echo $setting->entity_value; ?>" name="email_from" id="email_from" placeholder="example@email.com">
                                            </div>

                                        <?php }
                                    }

                                    ?>

                                    <button type="submit" id="" class="btn btn-default">Save</button>
                                    <button type="reset" id="" class="btn btn-warning">Cancel</button>
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="embedsettings" class="tab-pane fade <?php if(Session::get('tabs')=='embedsettings'){ echo 'in active';} ?>">
                        <div class="col-sm-12 feature-col">
                            <div id=""  class="tab-pane fade in active">
                                <h3>Embed Form Settings</h3>
                                <?php if(Session::has('messageembedsettings')) {
                                    ?>
                                    <div class="alert alert alert-success" role="alert">
                                        <?php echo Session::get('messageembedsettings'); ?>
                                    </div>
                                <?php } ?>
                                <form class="col-md-9" id="" action="<?php URL::to('shopify/settings');?>" method="post">
                                    <?php
                                        $embed_button_label='';
                                        foreach($settings as $setting){
                                                if($setting->entity_name == 'embed_button_label'){ $embed_button_label=$setting->entity_value;}
                                        }
                                    ?>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Enter Embed Form Button Label</label>
                                        <input type="text" class="form-control" value="<?php if($embed_button_label){echo $embed_button_label;}else{echo 'Submit';}; ?>" name="embed_button_label" id="embed_button_label" placeholder="">
                                    </div>

                                    <button type="submit" id="" class="btn btn-default">Save</button>
                                    <button type="reset" id="" class="btn btn-warning">Cancel</button>
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="tabs" value="embedsettings">

                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </div>



                   
                
        </div>
    </div>

</div>
</body>


</html>
	