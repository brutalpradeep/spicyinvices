<!DOCTYPE html>
<html>
<head>
<!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
window.__insp = window.__insp || [];
__insp.push(['wid', 573611976]);
(function() {
function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
})();
</script>
<!-- End Inspectlet Embed Code -->

    <?php echo Minify::stylesheet(array('/assets/select2.css', '/assets/bootstrap/css/bootstrap.min.css','/assets/font-awesome-4.0.3/css/font-awesome.min.css','/assets/bower/bootstrap-table/src/bootstrap-table.css','/assets/bootstrap-editable.css','/assets/bootstrap/css/bootstrap.min.css','/assets/select2-bootstrap.css','/assets/shopify.css','/assets/bower/spectrum/spectrum.css','/assets/datepicker.css'))->withFullUrl() ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
</head>
<body>
<script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
<script type="text/javascript">
    ShopifyApp.init({
        apiKey: '<?php echo $app_key; ?>',
        shopOrigin: 'https://<?php echo Session::get('shop_name'); ?>.myshopify.com',
        debug: false
    });
</script>

<div id="wrapper">
<?php echo \View::make('partials.menu')->render(); ?>
<?php echo isset($content)?$content:''; ?>
<?php echo  Minify::javascript(array('/assets/jquery.min.js', '/assets/bootstrap/js/bootstrap.min.js','/assets/jquery.validate.js','/assets/bower/bootstrap-table/src/bootstrap-table.js','/assets/bower/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js','/assets/bootstrap-datepicker.js','/assets/bootstrap-editable.js','/assets/select2.js','/assets/bower/ckeditor/ckeditor.js','/assets/bower/spectrum/spectrum.js','/assets/bootstrap-table-filter.js','/assets/filters.js','/assets/bower/bootstrap-table-filter/src/ext/bs-table.js','/assets/shopify.js'))->withFullUrl(); ?>
 <script>
        jQuery.ajax(
            {
                type: "post",
                url: "<?php echo \URL::to('/shopify/intercom'); ?>",
                success : function(data){
                    window.intercomSettings = {
                        //The current logged in user's full name
                        name: data.shop_name,
                        //The current logged in user's email address.
                        email: data.email,
                        //The current logged in user's sign-up date as a Unix timestamp.
                        created_at:  new Date(data.created_at).getTime()/1000,
                        app_id: "s5gkkklx"
                    };
                }
            });

    </script>
    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/s5gkkklx';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
</div>
</body>
</html>


