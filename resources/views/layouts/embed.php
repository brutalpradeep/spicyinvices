<!DOCTYPE html>
<html>
<head>
    <!-- End Inspectlet Embed Code -->
    <?php echo Minify::stylesheet(array('/assets/bootstrap/css/bootstrap.min.css','/css/fonts/font.css','/assets/shopify.css'))->withFullUrl() ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
</head>
<body class="iframe-body">
<div class="container content">
<?php echo isset($content)?$content:''; ?>
<?php echo  Minify::javascript(array('/assets/iframeResizer.contentWindow.min.js'))->withFullUrl(); ?>
</div>
</body>
</html>


