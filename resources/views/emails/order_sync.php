<div style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:25px;background:#fff;">

    Hey <?php echo $name;?>,<br />

    We are currently syncing Orders from <?php echo $shop_name;?>.<br />

    Syncing can take up sometime depending on the number of orders. Here are some time estimates for your reference:<br />
    <ol>
        <li>10,000 Orders - 45-60 minutes</li>
        <li>5000 Orders : 30-45 minutes</li>
        <li>1000- 5000 orders : 15-30 minutes</li>
        <li>Less than 1000 : Under 10 minutes </li>
    </ol>
    In the meantime you can test out the App with 30 orders.<br />

    Regards,<br />
    Ankit<br />
    Team SpiceGems

</div>