
<div style="font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:25px;background:#fff;">
    Hey <?php echo $name;?>,<br />

    Thanks for Installing Custom Order Status App.<br />

    With this App we have tried to make things easy for shops which sell custom made items.<br />

    The App is quite simple and straight forward. I bet it wont take you more than 10 minutes to master the interface.<br />

    In case you face any issues, you can always reach out to me at <a href="mailto:support@spicegems.com" target="_top">support@spicegems.com</a><br />

    I will be happy to help<br />

    Regards,<br />
    Ankit<br />
    Team SpiceGems<br />

	
</div>