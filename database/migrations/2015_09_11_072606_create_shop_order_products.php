<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOrderProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_order_products', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned();
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('variant_id')->unsigned();
            $table->integer('quantity');
            $table->text('name');
            $table->text('data');
            $table->timestamps();
            $table->foreign('order_id')->references('order_id')->on('shop_orders')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
