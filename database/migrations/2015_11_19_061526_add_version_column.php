<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;
class AddVersionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(Schema::hasTable('shop_users')){
		
			$columns = Schema::getColumnListing('shop_users');
			if(in_array('is_update',$columns)){
			
				 Schema::table('shop_users', function($table)
					{
						$table->dropColumn('is_update');
					});
			}            
            Schema::table('shop_users', function (Blueprint $table) {
                $config =  \MainConfig::$config;
                $table->string('version', '4')->default($config['App']['VERSION']);
            });
            DB::table('shop_users')->where('is_active', 1)->where('shop_name','!=', '')->update(array('version' => 1.00));

        };

        if(Schema::hasTable('shop')){
            Schema::table('shop', function (Blueprint $table) {
                $config =  \MainConfig::$config;
                $table->string('version', '4')->default($config['App']['VERSION']);
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
