<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_orders', function (Blueprint $table) {
            $table->bigInteger('order_id')->unsigned();
            $table->string('email');
            $table->dateTime('order_created_at');
            $table->dateTime('order_updated_at');
            $table->string('name');
            $table->decimal('total_price',10,2);
            $table->dateTime('processed_at');
            $table->string('fulfillment_status');
            $table->string('status');
            $table->string('financial_status');
            $table->integer('custom_status_id')->unsigned();
            $table->text('data');
            $table->timestamps();
            $table->primary(array('order_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
