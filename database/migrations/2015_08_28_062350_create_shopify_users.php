<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopifyUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('shop', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shop_name');
            $table->string('permanent_token');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });


        $connection =  Config::get('database');
        $config =  \MainConfig::$config;
        $db = (isset($connection['connections']['mysql']['database']))?$connection['connections']['mysql']['database']:$connection['default'];

        if($db ==  $config['DB']['DB_DATABASE']){

            if(!Schema::hasTable('shop_users')){
                Schema::create('shop_users', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('shop_name');
                    $table->string('permanent_token');
                    $table->string('database_name');
                    $table->tinyInteger('is_active')->default(1);
                    $table->timestamps();
                });
            }
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
