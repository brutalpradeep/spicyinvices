<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $customers = DB::table('shop_order_customers')->select('id')->distinct()->get();

        if(count($customers)>0)
        {
            foreach($customers as $customer)
            {
                $customer=(object)$customer;
                $customer_except = DB::table('shop_order_customers')->where('id',$customer->id)->orderBy('updated_at','desc')->first();
                $customer_except=(object)$customer_except;
                DB::table('shop_order_customers')->where('id',$customer_except->id)->where('email','!=',$customer_except->email)->delete();
            }
        }

        Schema::table('shop_order_customers', function (Blueprint $table) {
            $table->string('email')->change();
            $table->unique('id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return voidw
     */
    public function down()
    {
        //
    }
}
