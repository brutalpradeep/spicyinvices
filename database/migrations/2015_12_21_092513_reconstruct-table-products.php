<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReconstructTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connection =  Config::get('database');
        $config =  \MainConfig::$config;
        $db = (isset($connection['connections']['mysql']['database']))?$connection['connections']['mysql']['database']:$connection['default'];

        if($db !=  $config['DB']['DB_DATABASE']){
            if (Schema::hasColumn('shop_order_products', 'id')){
                    Schema::table('shop_order_products', function (Blueprint $table) {
                        $table->dropColumn('id');
                    });
            }

            Schema::table('shop_order_products', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('shop_id')->unsigned();
                $table->bigInteger('product_id')->unsigned();
                $table->bigInteger('shop_product_id')->unsigned();
                $table->text('title');
                $table->string('sku');
                $table->text('variant_title');
                $table->text('total_discount');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
