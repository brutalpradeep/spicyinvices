<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRowShopEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $shop_email_address =DB::table('shop_settings')->where('entity_name', 'shop_email_address')->select('entity_value')->first();
		
        if(!$shop_email_address)
        {
            $shop_email =DB::table('shop_settings')->where('entity_name', 'email_from')->first();
			if(is_array($shop_email)){
			  $shop_email = (object)$shop_email;
			}
			
           if(isset($shop_email->entity_value)){

                DB::table('shop_settings')->insert(
                    array('entity_name'=>'shop_email_address','entity_value'=>$shop_email->entity_value,'created_at'=>$shop_email->created_at,'updated_at'=>$shop_email->updated_at)
                );
            }

        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
