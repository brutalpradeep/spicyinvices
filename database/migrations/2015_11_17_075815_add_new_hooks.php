<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewHooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('shop_users')){
            Schema::table('shop_users', function (Blueprint $table) {
                $table->text('new_hooks');
           });
            DB::statement("UPDATE `shop_users`  set new_hooks = IFNULL (CONCAT( new_hooks , ',customers/update,customers/create' ), 'customers/update,customers/create') where is_update=1");

        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
