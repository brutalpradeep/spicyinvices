<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColomnAdminNote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connection =  Config::get('database');
        $config =  \MainConfig::$config;
        $db = (isset($connection['connections']['mysql']['database']))?$connection['connections']['mysql']['database']:$connection['default'];

        if($db !=  $config['DB']['DB_DATABASE']){

            Schema::table('shop_orders', function (Blueprint $table) {

            $table->text('admin_note');
        });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
