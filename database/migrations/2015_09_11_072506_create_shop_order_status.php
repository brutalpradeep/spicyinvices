<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOrderStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_order_status', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('internal_name');
            $table->string('external_name');
            $table->string('color','6');
            $table->tinyInteger('is_email')->default(1);
            $table->tinyInteger('is_delete');
            $table->tinyInteger('is_active');
            $table->text('email_message');
            $table->text('email_subject');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
