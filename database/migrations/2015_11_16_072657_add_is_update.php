<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('shop_users')){
            Schema::table('shop_users', function (Blueprint $table) {
                $table->tinyInteger('is_update')->default(0);
            });

            DB::table('shop_users')->where('is_active', 1)->where('shop_name','!=', '')->update(array('is_update' => 1));
        };

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
