<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSizeCustomerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connection =  Config::get('database');
        $config =  \MainConfig::$config;
        $db = (isset($connection['connections']['mysql']['database']))?$connection['connections']['mysql']['database']:$connection['default'];

        if($db !=  $config['DB']['DB_DATABASE']){

            Schema::table('shop_orders', function (Blueprint $table) {
                $table->bigInteger('customer_id')->change();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
