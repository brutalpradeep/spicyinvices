<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBeingSyncedInShopUsers extends Migration
{
    /**
     * Run the migrations.
     * Added Column being_ynced In Shop_Users table .
     * @return void
     */
    public function up()
    {
        $connection =  Config::get('database');
        $config =  \MainConfig::$config;
        $db = (isset($connection['connections']['mysql']['database']))?$connection['connections']['mysql']['database']:$connection['default'];

        if($db ==  $config['DB']['DB_DATABASE']){

        if(Schema::hasTable('shop_users')){
            Schema::table('shop_users', function($table)
            {
                $table->string('being_synced')->default(0);
            });
        }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
