<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use App\Models;
class UpdadeOrdersAddCustomerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $orders = DB::table("shop_orders")->get();

        if ($orders) {
            foreach ($orders as $order) {
                $order=(object)$order;
                if ($order->email != '') {
                    $customer = DB::table("shop_order_customers")->where('email', $order->email)->first();
                    $customer=(object)$customer;

                    if ($customer && isset( $customer->id)) {
                        DB::table("shop_orders")->where('order_id', $order->order_id)->update(['customer_id' => $customer->id]);
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
