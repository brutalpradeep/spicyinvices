$(document).ready(function () {


    var email_default_text ="HI [customer-name],<br><br>We are working hard to fulfill your order. Here are the details of your order.<br>Order Number: [order-number]<br> Custom Status : [custom-status]<br><br> Regards,<br>[shop-name]";
    var email_additional_noti_email ="Hi,<br><br> The status of [order-number] has changed to [custom-status] <br><br> Thanks,<br> [shop-name]";

    var custom_status_div = $("#custom_status_div");
    var all_orders_status_div = $("#all_orders_status_div");
    var add_custom_status = $("#add_custom_status");
    var hide_add_order_status = $("#hide_add_order_status");
    var is_email = $("#is_email");
    var frm_order_status = $("#frm_order_status");
    var email_notification = $(".email_notification");
    var table = $("#table");
	var total_order_rows=11;

    var dept_editor;
    var is_dept_email =$("#is_email_department");
    var dept_email_section =$(".email_department");
    var validator;
    var shop_name =$("#shop_name").text();

    table.bootstrapTable();

    /* Add CKEditor to add new status textarea */
    var editor;
    var load_editor = function (name) {
        if ($('#'+name).length) {
            editor = CKEDITOR.instances[name];
            if(editor)
            {

                editor.destroy(true);

                CKEDITOR.remove(editor);

            }
            editor = CKEDITOR.replace(name,{
                toolbar: [
                    ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
                    ['Styles','Format','Font','FontSize'],
                    [ 'Link','Unlink','Anchor' ,'Table','-','Source'],
                    [ 'HorizontalRule','SpecialChar' ,'-','NumberedList','BulletedList'],
                    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
                ]
            });
        }
        return editor;
    };
   editor =  load_editor('email_message');

    /* Add New Custom Status Called */
    add_custom_status.click(function(){
        all_orders_status_div.hide();
        custom_status_div.slideDown(0);
        set_form_reset();
        email_notification.slideDown(50);
        create_colorPicker();
      //  is_email.trigger('click');
        is_email.attr('checked','checked');

        dept_email_section.hide();
        is_dept_email.removeAttr('checked');
        load_editor('email_message');
         editor.setData(email_default_text);



    });

    /* show all status */
    hide_add_order_status.on('click',function(){
        $(".all_div").hide();
        all_orders_status_div.slideDown(0);
    });

    /* show/hide email subject, message on click email notification */
    is_email.on('click',function(){
        if($(this).prop('checked')==true){
            email_notification.slideDown(50);

            load_editor('email_message');

            var data = editor.getData();
            if(!data){
                editor.setData(email_default_text);

            }
        }
        else{  email_notification.slideUp(50);  }
    });


    /* Save New Custom order Status */

    validator = $('#frm_order_status').validate({

        rules: {
            internal_name: {

                required: true
            },
            external_name: {

                required: true
            },
            department_email: {
                required:true,
                check_multiple_email:true
            }


        }, highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function() {
           
            var data = frm_order_status.serializeArray();


            if(frm_order_status.find(".email_notification").css('display')=='block') {
				$.each(data,function(i,obj){
					if(obj.name=='email_message'){
						data[i]['value'] = editor.getData();
					}
				});

            }

            if(frm_order_status.find(".email_department").css('display')=='block') {
                $.each(data,function(i,obj){
                    if(obj.name=='department_email_message'){
                        data[i]['value'] = dept_editor.getData();
                    }
                });

            }

            var i = $("#index").val();
            var action = $("#order_status_action").val();
            var response = {};
            jQuery.ajax({
                method: "POST",
                url: "status/create",
                data: data,
                success: function (data) {
                    if (data != '') {
						if(typeof data =='object')
						{
							response = data;
						}else{
							response = jQuery.parseJSON(data);
						}
						
                        if (action == 'update') {
                            table.bootstrapTable('updateRow', {index: i, row: create_row(response)});
                        }
                        else {
                            table.bootstrapTable('append', create_row(response));
                        }
                        update_attribute();
                    }
                }
            });

            set_form_reset();
            hide_add_order_status.trigger('click');
            return false;
        }
    });

    var total_row=11;
    create_row = function(response)
    {
        var is_email='No';
        var is_active='Disable';
        if(response['is_email']==1){ is_email='Yes'; }
        if(response['is_active']==1){ is_active='Enabled'; }


        var row= {
            "id": ++total_row,
            "internal_name": response['internal_name'],
            "external_name": response['external_name'],
            "color":  '<a style="background-color: #'+response['color']+'" href="javascript:void(0)" class="color">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>',
            "email_notifications":is_email,
            "status":'<div><a href="javascript:void(0)" data-value="'+response['is_active']+'" data-title="Select status" class="disable_order_status" data-pk="'+response['id']+'">'+ is_active+'</a></div>',
            "actions": '<div style="float: left"><a href="javascript:void(0)" class="edit_status" data-id="'+response['id']+'">Edit</a></div>'
        };
        return row;
    }


    /* edit order status */
    $(document).on('click',".edit_status", function () {
        var id=$(this).data('id');
        var index =$(this).parents('tr').data('index');
        var val={};
        jQuery.ajax({
            method:"get",
            url:"status/edit",
            data:{"id":id},
            success:function(data)
            {
                val = jQuery.parseJSON(data);
                edit_order_status_data(val,index);
            }
        });
    });


    /* edit order status div show */
    edit_order_status_data = function (data,index) {
        var custom_status_div = $("#custom_status_div");

        create_colorPicker(data['color']);
        $(".all_div").hide();
        custom_status_div.slideDown(0);
        set_form_reset();
        $("input[name=order_status_id]").val(data['id']);
        $("#internal_name").val(data['internal_name']);
        $("#external_name").val(data['external_name']);
        $("#color").val(data['color']);
        $("#order_status_action").val("update");
        $("#index").val(index);

        if(data['is_email']==1){
            is_email.attr('checked','checked');
            $(".email_notification").show();
            $("#email_subject").val(data['email_subject']);

            load_editor('email_message');
			editor.setData(data['email_message']);

        }else{
            $(".email_notification").hide();
            is_email.removeAttr("checked");

        }
		
        if(data['is_email_department']==1){
            is_dept_email.attr('checked','checked');
            $(".email_department").show();
            $("#department_email").val(data['department_email']);
            $("#department_email_subject").val(data['department_email_subject']);

            load_dept_editor('department_email_message');
			dept_editor.setData(data['department_email_message']);
        }else{
            $(".email_department").hide();
            is_dept_email.removeAttr("checked");
        }

    }

    $(document).on('click',"#button",function(){
        var result = confirm("Are you sure want to delete ?");
        var ids = $.map(table.bootstrapTable('getSelections'), function (row) {
            edit_row=row.actions;
            return $(edit_row).find('.edit_status').data('id');
        });
        var indexes = $.map(table.bootstrapTable('getSelections'), function (row) {
           return row.id;
        });

        if(result==true)
        {
            jQuery.ajax({
                method:"POST",
                url:"status/delete",
                data:{"ids":ids},
                success:function(data)
                {
                    table.bootstrapTable('remove', { field: 'id', values: indexes });
                    update_attribute();
                }
            });
        }
    });


    var set_form_reset = function (add) {
        frm_order_status[0].reset();

            editor.setData('');
            dept_editor.setData('');

      //  email_notification.hide();
        $("#order_status_action").val('');
    }

     var disable_order_status = function () {
        var source = [{'id': '1', 'text': 'Enabled'},{'id': '0', 'text': 'Disabled'}];
        var is_active={'0':'Disabled','1':'Enabled'};
        $("a.disable_order_status").each(function (index) {
            var tr = $(this).parents('tr');
            var i = tr.data('index');
            $(this).editable({
                type: 'select2',
                url:"status/disable",
                source:source,
                success: function (response, newValue) {
                    var data =JSON.parse(response);
                    data = data[0];
                    var update_row='<div><a href="javascript:void(0)" data-value="'+data['is_active']+'" data-title="Select status" class="disable_order_status" data-pk="'+data['id']+'">'+ is_active[newValue]+'</a></div>';
                    table.bootstrapTable('updateRow', {index: i, row: {'status' : update_row }});
                }
            });

            if($(this).data('value')==1){
                tr.removeClass('disable');
            }
            else{
                tr.addClass('disable');
            }

        });
    };
    disable_order_status();

    table.on('all.bs.table', function () {
        disable_order_status();
    });

    function create_colorPicker(color)
    {
        var element =$("#color");
        element.spectrum({
            showInput: true,
            preferredFormat: "hex",
            color: color
        });

        $("#btnEnterAColor").click(function() {
            //table.bootstrapTable('updateRow', {index: 1, row: row});
            element.spectrum("set", $("#enterAColor").val());
        });

    }

    update_attribute = function()
    {
        table.find("tbody >tr").each(function(i,ele){
            var index=$(ele).find('input[name=btSelectItem]').data('index');
            table.bootstrapTable('updateRow',{index: index, row: {id:++index}});
        });
    }


var $table = $('#order');
$table.bootstrapTable({
    columns: [
        {
            field: 'state',
            align: 'center',
            checkbox:true
        },
        {
            field: 's_no',
            title: 'S. No.',
            align: 'center',
            width: '50px'
        },
        {
            field: 'name',
            title: 'Order',
            sortable: true,
            editable: false,
            align: 'center',
            showFilter:true,
            formatter:LinkCreater
        },

        {
            field: 'order_created_at',
            title: 'Date',
            sortable: true,
            align: 'center',
            editable: false,
            width: '100px'
        },

        {
            field: 'customer_name',
            title: 'Customer',
            sortable: false,
            align: 'center'
        },
        {
            field: 'custom_status_id',
            title: 'Custom Status',
            sortable: true,
            align: 'center',
            formatter:LinkFormatter

        } ,
        {
        field: 'status',
        title: 'Status',
        sortable: true,
        align: 'center',
        editable: false,
        showFilter:true
        },
        {
        field: 'fulfillment_status',
        title: 'Fulfillment Status',
        sortable: true,
        align: 'center',
        editable: false
        },
        {
        field: 'financial_status',
        title: 'Payment Status',
        sortable: true,
        align: 'center',
        editable: false
        }
]


});
 set_editable_status = function () {
        $( "a.status" ).each(function( index ) {
            $( this).editable({
                type: 'select2',
                source:source,
                success:function(response,newValue)
                {
					if(typeof response =='object')
					{
						data = response;
					}else{
						data = jQuery.parseJSON(response);
					}
					
                    $(this).css('background','#'+data.background);
                    $(this).css('color','#'+data.color);
					$(this).attr('data-value',data.status_id);
                }

            });
        });
    };
    set_editable_status();
	
	/* Fetch Order status on background */
    get_custom_status = function () {
		if(status_rows>count_status)
		{
			jQuery.ajax({
				method:"GET",
				data:{
				'action':'ajax_response'
				},
				url:page_path+"/shopify/orders/status",
				success:function(data)
				{
					var data=JSON.parse(data);
					$.each(data,function(i,obj){
						table.bootstrapTable('append', create_row(obj));
					});
				}
			});
		}
    };
    if(typeof(page_path) != "undefined" && page_path !== null){
        get_custom_status();
    }
	
	function LinkFormatter(value, row, index) {
        return '<a href="javascript:void(0)" class="status" data-pk="'+row.order_id+'" data-url="process" data-title="Select status"  data-name="custom_status_id" data-value="'+value+'" style="background: #'+row.color+'; color:#'+row.text_color+'">'+row.custom_status_name+'</a>';
    }

    function LinkCreater(value, row, index)
    {
        return '<a href="javascript:void(0)" class="show_order_details" data-order_id="'+row.order_id+'" title="Show Details">'+value+'</a>'+
            '<a target="_blank" class="ext-icon" title="View on Shopify" href="https://'+shop_name+'.myshopify.com/admin/orders/'+row.order_id+' " ><i class="fa fa-external-link"></i></a>';
    }
	
	
var url = window.location;
// Will only work if string in href matches with location
$('ul.nav a').parent().removeClass('active');
$('ul.nav a[href="'+ url +'"]').parent().addClass('active');

// Will also work for relative and absolute hrefs
$('ul.nav a').filter(function() {
    return this.href == url;
}).parent().addClass('active');



    $table.on('all.bs.table', function () {
       set_editable_status();

});

    $('#filter-bar').bootstrapTableFilter({
        connectTo: '#order',
        filters:[
            {
                field: 'custom_status_id',    // field identifier
                label: 'Custom Status',    // filter label
                type: 'select',
                values: (typeof custom_status != 'undefined' )?custom_status:''
            },
            {
                field: 'status',
                label: 'Order Status',
                type: 'select',
                values: (typeof order_status != 'undefined' )?order_status:''

               // filter is visible by default
            },
            {
                field: 'fulfillment_status',
                label: 'Fulfillment Status',
                type: 'select',
                values: (typeof fulfillment_status != 'undefined' )?fulfillment_status:''

                // filter is visible by default
            },
            {
                field: 'financial_status',
                label: 'Payment Status',
                type: 'select',
                values: (typeof payment_status != 'undefined' )?payment_status:''

                // filter is visible by default
            },
            {
                field: 'order_created_at',
                label: 'Date',
                type: 'range',
                check : function(filterData, value) {
                console.log(filterData)
                if (typeof filterData.lte !== 'undefined' && (value) > (filterData.lte)) {
                    return false;
                }
                if (typeof filterData.gte !== 'undefined' && (value) < (filterData.gte)) {
                    return false;
                }
                if (typeof filterData.eq !== 'undefined' && (value) != (filterData.eq)) {
                    return false;
                }
                return true;
            }

                // filter is visible by default
            }
        ],
        onSubmit: function() {

        },
        onAll: function() {

            $('.date').datepicker({
                format:'yyyy-mm-dd',

                autoclose: true
            });

        }


    });
	

	
	/* Create New Row for Order Table */
    create_orders_row = function (data) {
        var custom_status='Empty';
        if(data.custom_status_id!=0)
        {
            custom_status = data.custom_status_id;
        }
        
        var row= {
            "order_id": ++total_order_rows,
            "name": '<a target="_blank"  href="https://'+shop_name+'.myshopify.com/admin/orders/'+data.order_id+'">'+data.name+'</a>',
            "customer_name": data.customer_name,
            "custom_status_id":'<a href="javascript:void(0)" class="status  editable editable-click" data-pk="'+data.order_id+'" '+
            'data-url="'+page_url+'/shopify/process" data-title="Select status" data-type="select" data-name="custom_status_id" data-value="'+data.custom_status_id+'"'+
            ' style="background: #'+data.color+'; color:#'+data.text_color+'">'+data.custom_status_name+'</a>',
            "status": data.status,
            "fulfillment_status": data.fulfillment_status,
            "financial_status": data.financial_status,
            "total_price": data.total_price,
            "order_created_at": data.order_created_at
        };
        return row;
    };
    
		
	/* Fetch Orders on background */
    get_orders_on_background = function (data) {
			if(typeof(orders) != "undefined" && orders !== null){
				$.each(orders,function(i,obj){
					$table.bootstrapTable('append', create_orders_row(obj));
				});
			}
			jQuery.ajax({
				method:"POST",
				url:page_url+"/shopify/orders/get",
				data:{
				'limit':'',

				'offset':30

				},
				success:function(data)
				{
					if(data!='')
					{
						var data=JSON.parse(data);
						$.each(data['rows'],function(i,obj){
							$table.bootstrapTable('append', create_orders_row(obj));
						});
					}
				}
			});
    };
    if(typeof(page_name) != "undefined" && page_name !== null){
		get_orders_on_background();
    }


    var load_dept_editor = function (name) {
        if ($('#'+name).length) {
            dept_editor = CKEDITOR.instances[name];
            if(dept_editor)
            {
                dept_editor.destroy(true);
            }
            dept_editor = CKEDITOR.replace(name,{
                toolbar: [
                    ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
                    ['Format','Font','FontSize'],
                    [ 'Link','Unlink','Anchor' ,'-','Source'],
                    [ 'HorizontalRule','SpecialChar' ,'-','NumberedList','BulletedList'],
                    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
                ]
            });
        }
    };
    load_dept_editor('department_email_message');

    is_dept_email.on('click',function(){
        if($(this).prop('checked')==true){
            dept_email_section.slideDown(50);
            load_dept_editor('department_email_message');
            var data = dept_editor.getData();
            if(!data){
                dept_editor.setData(email_additional_noti_email);
            }
        }
        else{  dept_email_section.slideUp(50);  }
    });

    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

    jQuery.validator.addMethod("check_multiple_email", function (value, element) {
        if (this.optional(element)) {
            return true;
        }
        var emails = value.split(','),
            valid = true;

        for (var i = 0, limit = emails.length; i < limit; i++) {
            value = emails[i].trim();
			
            valid = valid && jQuery.validator.methods.email.call(this, value, element);
        }

        return valid;
    }, "Invalid email format: please use a comma to separate multiple email addresses.");

    $("#redirect_to_shop").on('click',function(){
        var shop_name =$("#shop_name").val();
        var app_url =$("#app_url").val();
        if(shop_name!='')
        {
            window.top.location.href=app_url+"?shop="+shop_name;
        }
        else{
            $("#error_msg").html("Please Enter shop url");
            $("#shop_form_id").addClass("has-error");
        }
    });

    $(document).on("click",".show_order_details",function(){

        var order_id = $(this).data("order_id");

        $.ajax({
            method:"POST",
            url:"order/details",
            data:{
                order_id:order_id
            },
            success:function(data)
            {
                $(".orders_all").hide();
                $("#order_details").html(data).fadeIn();
                $("#order_details").find(".show_on_shopify").attr("href","https://"+shop_name+".myshopify.com/admin/orders/"+order_id);
                $("#admin_notes").editable({ showbuttons: 'bottom' });
            }
        });



    });
    $(document).on("click",".back_to_orders",function(){
        $("#order_details").hide();
        $(".orders_all").fadeIn();
    });

    if($('.orders_all .search input').length)
    {
        var $search = $('.orders_all .search input');
        $search.attr('placeholder', 'Search for custom status etc.');
    }


    /* To show Bulk actions in order page */
    var bulk_actions = $(".bulk_actions");
    $(document).on('change keydown', '.bs-checkbox input[name=btSelectAll], .bs-checkbox input[name=btSelectItem]',function(){
        if($(this).is(':checked'))
        {
            bulk_actions.attr('style','display:inline-block');
        }
        else
        {
            var boxes = $('.bs-checkbox input[name=btSelectItem]:checked');
            if(boxes.length==0){
                bulk_actions.attr('style',"display:none");
            }
        }
    });
    /* Show modal on dropdown click */
    bulk_actions.find("ul>li>a").on("click",function(){
        var value = $(this).data('value');
        var status_all = $("#status_all");
        var icon='<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>';
        if(value=='custom_status_id')
        {
            status_all.modal().find(".all_div").hide();
            status_all.find(".status_div").show();
            status_all.find(".info_box").html(icon +'This will bulk update the custom status of selected orders.');
            var options ='';
            $.each(custom_status,function(i,ele){
                options += '<option value="'+ele.id+'">'+ele.label +'</option>';
            })
            $("#change_all_status").html(options);
        }
        else if(value=='admin_note')
        {
            status_all.modal().find(".all_div").hide();
            status_all.find(".note_div").show();
            status_all.find(".info_box").html(icon+'This will Bulk Update the Notes for Selected Order. This will overwrite the previously written note if any.');
        }
        status_all.find("#action").val(value)
    });
    /* update data of bulk actions */
    $(".status_all_btn").on('click',function(){
        var data = $('form[name=all_settings]').serializeArray();
        var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.order_id;
        });
        var actions = $("#status_all").find("#action").val();

        jQuery.ajax({
            method:"POST",
            url:"bulk/actions",
            data:{"ids":ids,'data':data,'actions':actions},
            success:function(data)
            {
                location.reload();
            }
        });

    });




});
